package secure;

import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Clase encargada de realizar los procesos de encriptado, desencriptado y hashing
 */
public class Crypto {
    /**
     * Declaración de atributos imprescindibles para los procesos de seguridad. Entre ellos, las claves que se reciben
     * de la clase Main: mensaje a encriptar y desencriptar
     */
    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
    Key key;
    private String texto, clave, textoHash1, textoHash2;

    Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");

    /**
     * Constructor para inicializar los parámetros
     *
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     */
    public Crypto() throws NoSuchAlgorithmException, NoSuchPaddingException {
        this.clave = "";
        this.texto = "";
    }

    /**
     * Método por el que se realiza el encriptado con los parámetros recibidos. Además se codifica para facilitar la
     * labor de envío
     *
     * @param clave
     * @param texto
     * @return
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */

    public String encriptacion(String clave, String texto) throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        keyGenerator.init(256);
        key = keyGenerator.generateKey();
        key = new SecretKeySpec(clave.getBytes(), 0, 16, "AES");
        aes.init(Cipher.ENCRYPT_MODE, key);
        byte[] encriptado = aes.doFinal(texto.getBytes());
        String codificado = Base64.getEncoder().encodeToString(encriptado);

        System.out.println();
        return codificado;
    }

    /**
     * Método por el que se realiza el desencriptado con los parámetros recibidos y decodificado.
     * @param clave
     * @param codificado
     * @return
     * @throws InvalidKeyException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */

    public String desencriptado(String clave, String codificado) throws InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException {
        keyGenerator.init(256);
        key = keyGenerator.generateKey();
        key = new SecretKeySpec(clave.getBytes(), 0, 16, "AES");
        byte [] encriptado= Base64.getDecoder().decode(codificado);

        aes.init(Cipher.DECRYPT_MODE, key);

        byte[] desencriptado = aes.doFinal(encriptado);

        return new String(desencriptado);
    }

    /**
     * Método por el que se realiza el Hashing, retornando la clave de hash
     *
     * @param passwordToHash
     */

    public String hashingInTheNight (String passwordToHash){
        try {
            MessageDigest md=MessageDigest.getInstance("SHA-512");
            md.update(passwordToHash.getBytes());
            byte [] mb= md.digest();
            textoHash1=HexBin.encode(mb);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return textoHash1;
    }

    /**
     * Método de verificación del hash en el que se compara el hash con un hasheo de otro parámetro.
     * @param textoNuevo
     * @param hash
     * @return
     */
    public boolean verificarHash (String textoNuevo, String hash) {

        return (hashingInTheNight(hash).equalsIgnoreCase(textoNuevo));

    }
}
