package hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Cliente {
    private int id;
    private String nombre;
    private String cif;
    private String dccion;
    private int cp;
    private String poblacion;
    private String provincia;
    private int tfno;
    private List<Viajespropios> viajes;
    private List<Viajesvendidos> vendidos;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "cif")
    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    @Basic
    @Column(name = "dccion")
    public String getDccion() {
        return dccion;
    }

    public void setDccion(String dccion) {
        this.dccion = dccion;
    }

    @Basic
    @Column(name = "cp")
    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    @Basic
    @Column(name = "poblacion")
    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    @Basic
    @Column(name = "provincia")
    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @Basic
    @Column(name = "tfno")
    public int getTfno() {
        return tfno;
    }

    public void setTfno(int tfno) {
        this.tfno = tfno;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return id == cliente.id && cp == cliente.cp && tfno == cliente.tfno && Objects.equals(nombre, cliente.nombre) && Objects.equals(cif, cliente.cif) && Objects.equals(dccion, cliente.dccion) && Objects.equals(poblacion, cliente.poblacion) && Objects.equals(provincia, cliente.provincia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, cif, dccion, cp, poblacion, provincia, tfno);
    }

    @OneToMany(mappedBy = "cliente")
    public List<Viajespropios> getViajes() {
        return viajes;
    }

    public void setViajes(List<Viajespropios> viajes) {
        this.viajes = viajes;
    }

    @OneToMany(mappedBy = "vendido")
    public List<Viajesvendidos> getVendidos() {
        return vendidos;
    }

    public void setVendidos(List<Viajesvendidos> vendidos) {
        this.vendidos = vendidos;
    }
}
