package hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Viajesvendidos {
    private int id;
    private String origen;
    private String destino;
    private double precio;
    private double peso;
    private String facturado;
    private Date fecha;
    private String albaran;
    private Cliente vendido;
    private Proveedor proveedor;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "origen")
    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    @Basic
    @Column(name = "destino")
    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "peso")
    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    @Basic
    @Column(name = "facturado")
    public String getFacturado() {
        return facturado;
    }

    public void setFacturado(String facturado) {
        this.facturado = facturado;
    }

    @Basic
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "albaran")
    public String getAlbaran() {
        return albaran;
    }

    public void setAlbaran(String albaran) {
        this.albaran = albaran;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Viajesvendidos that = (Viajesvendidos) o;
        return id == that.id && Double.compare(that.precio, precio) == 0 && Double.compare(that.peso, peso) == 0 && Objects.equals(origen, that.origen) && Objects.equals(destino, that.destino) && Objects.equals(facturado, that.facturado) && Objects.equals(fecha, that.fecha) && Objects.equals(albaran, that.albaran);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, origen, destino, precio, peso, facturado, fecha, albaran);
    }

    @ManyToOne
    @JoinColumn(name = "idcliente", referencedColumnName = "id", nullable = false)
    public Cliente getVendido() {
        return vendido;
    }

    public void setVendido(Cliente vendido) {
        this.vendido = vendido;
    }

    @ManyToOne
    @JoinColumn(name = "idproveedor", referencedColumnName = "id")
    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }
}
