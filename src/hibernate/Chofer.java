package hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Chofer {
    private int id;
    private String nombre;
    private String apellidos;
    private Date fechanac;
    private Date fechainicio;
    private Date fechafin;
    private int nsegsocial;
    private String dni;
    private int tfno;
    private String dccion;
    private List<Viajespropios> viajes;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "fechanac")
    public Date getFechanac() {
        return fechanac;
    }

    public void setFechanac(Date fechanac) {
        this.fechanac = fechanac;
    }

    @Basic
    @Column(name = "fechainicio")
    public Date getFechainicio() {
        return fechainicio;
    }

    public void setFechainicio(Date fechainicio) {
        this.fechainicio = fechainicio;
    }

    @Basic
    @Column(name = "fechafin")
    public Date getFechafin() {
        return fechafin;
    }

    public void setFechafin(Date fechafin) {
        this.fechafin = fechafin;
    }

    @Basic
    @Column(name = "nsegsocial")
    public int getNsegsocial() {
        return nsegsocial;
    }

    public void setNsegsocial(int nsegsocial) {
        this.nsegsocial = nsegsocial;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "tfno")
    public int getTfno() {
        return tfno;
    }

    public void setTfno(int tfno) {
        this.tfno = tfno;
    }

    @Basic
    @Column(name = "dccion")
    public String getDccion() {
        return dccion;
    }

    public void setDccion(String dccion) {
        this.dccion = dccion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chofer chofer = (Chofer) o;
        return id == chofer.id && nsegsocial == chofer.nsegsocial && tfno == chofer.tfno && Objects.equals(nombre, chofer.nombre) && Objects.equals(apellidos, chofer.apellidos) && Objects.equals(fechanac, chofer.fechanac) && Objects.equals(fechainicio, chofer.fechainicio) && Objects.equals(fechafin, chofer.fechafin) && Objects.equals(dni, chofer.dni) && Objects.equals(dccion, chofer.dccion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, fechanac, fechainicio, fechafin, nsegsocial, dni, tfno, dccion);
    }

    @OneToMany(mappedBy = "chofer")
    public List<Viajespropios> getViajes() {
        return viajes;
    }

    public void setViajes(List<Viajespropios> viajes) {
        this.viajes = viajes;
    }
}
