package hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Tractora {
    private int id;
    private String matricula;
    private Date itv;
    private Date limitador;
    private Date tacografo;
    private Date seguro;
    private String tarjetatte;
    private List<Viajespropios> viajes;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "matricula")
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Basic
    @Column(name = "itv")
    public Date getItv() {
        return itv;
    }

    public void setItv(Date itv) {
        this.itv = itv;
    }

    @Basic
    @Column(name = "limitador")
    public Date getLimitador() {
        return limitador;
    }

    public void setLimitador(Date limitador) {
        this.limitador = limitador;
    }

    @Basic
    @Column(name = "tacografo")
    public Date getTacografo() {
        return tacografo;
    }

    public void setTacografo(Date tacografo) {
        this.tacografo = tacografo;
    }

    @Basic
    @Column(name = "seguro")
    public Date getSeguro() {
        return seguro;
    }

    public void setSeguro(Date seguro) {
        this.seguro = seguro;
    }

    @Basic
    @Column(name = "tarjetatte")
    public String getTarjetatte() {
        return tarjetatte;
    }

    public void setTarjetatte(String tarjetatte) {
        this.tarjetatte = tarjetatte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tractora tractora = (Tractora) o;
        return id == tractora.id && Objects.equals(matricula, tractora.matricula) && Objects.equals(itv, tractora.itv) && Objects.equals(limitador, tractora.limitador) && Objects.equals(tacografo, tractora.tacografo) && Objects.equals(seguro, tractora.seguro) && Objects.equals(tarjetatte, tractora.tarjetatte);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, matricula, itv, limitador, tacografo, seguro, tarjetatte);
    }

    @OneToMany(mappedBy = "tractora")
    public List<Viajespropios> getViajes() {
        return viajes;
    }

    public void setViajes(List<Viajespropios> viajes) {
        this.viajes = viajes;
    }
}
