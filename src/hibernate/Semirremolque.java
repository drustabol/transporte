package hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Semirremolque {
    private int id;
    private String matricula;
    private Date itv;
    private String tipo;
    private Date seguro;
    private List<Viajespropios> viajes;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "matricula")
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Basic
    @Column(name = "itv")
    public Date getItv() {
        return itv;
    }

    public void setItv(Date itv) {
        this.itv = itv;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "seguro")
    public Date getSeguro() {
        return seguro;
    }

    public void setSeguro(Date seguro) {
        this.seguro = seguro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Semirremolque that = (Semirremolque) o;
        return id == that.id && Objects.equals(matricula, that.matricula) && Objects.equals(itv, that.itv) && Objects.equals(tipo, that.tipo) && Objects.equals(seguro, that.seguro);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, matricula, itv, tipo, seguro);
    }

    @OneToMany(mappedBy = "semirremolque")
    public List<Viajespropios> getViajes() {
        return viajes;
    }

    public void setViajes(List<Viajespropios> viajes) {
        this.viajes = viajes;
    }
}
