package enums;

public enum TipoPlataformas {
    TAUTLINER("Tautliner"),
    FRIGO("Frigo"),
    CAJA("Caja"),
    CAJAABIERTA("Caja abierta");

    private String valor;

    TipoPlataformas(String valor) {
        this.valor=valor;
    }

    public String getValor() {
        return valor;
    }

}
