package gui;

import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

public class Controlador implements ActionListener, ListSelectionListener {

    private VistaUsuario vistaUsuario;
    private Modelo modelo;

    public Controlador(Modelo modelo, Vista vista) {

        this.vistaUsuario = vistaUsuario;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener((MouseListener) this);
    }

    private void addListSelectionListener(MouseListener listener) {
        vistaUsuario.listaInstrumentos.addListSelectionListener(listener);
        vistaUsuario.listaMarcas.addListSelectionListener(listener);
        vistaUsuario.listaMateriales.addListSelectionListener(listener);
        vistaUsuario.listaPropietarios.addListSelectionListener(listener);
        vistaUsuario.listaFabricados.addListSelectionListener(listener);
        vistaUsuario.ListaInstrumentosPropietario.addListSelectionListener(listener);
    }

    private void addActionListeners(ActionListener listener) {
        vistaUsuario.btnAddCliente.addMouseListener((MouseListener) listener);
        vistaUsuario.eliminarInstrumentoBtn.addActionListener(listener);
        vistaUsuario.eliminarInstrumentoBtn.setActionCommand("eliminarInstrumentoBtn");
        vistaUsuario.modificarInstrumentosBtn.addActionListener(listener);
        vistaUsuario.modificarInstrumentosBtn.setActionCommand("modificarInstrumentosBtn");
        vistaUsuario.btnlistadoInstrumentos.addActionListener(listener);
        vistaUsuario.btnlistadoInstrumentos.setActionCommand("btnlistadoInstrumentos");

        //Listener de materiales
        vistaUsuario.addMaterialBtn.addActionListener(listener);
        vistaUsuario.addMaterialBtn.setActionCommand("addMaterialBtn");
        vistaUsuario.eliminarMaterialBtn.addActionListener(listener);
        vistaUsuario.eliminarMaterialBtn.setActionCommand("eliminarMaterialBtn");
        vistaUsuario.modificarMaterialBtn.addActionListener(listener);
        vistaUsuario.modificarMaterialBtn.setActionCommand("modificarMaterialBtn");
        vistaUsuario.rbtnNo.addActionListener(listener);
        vistaUsuario.rbtnSi.addActionListener(listener);
        vistaUsuario.btnlistadoMateriales.addActionListener(listener);
        vistaUsuario.btnlistadoMateriales.setActionCommand("btnlistadoMateriales");

        //Listener de marcas
        vistaUsuario.addMarcaBtn.addActionListener(listener);
        vistaUsuario.addMarcaBtn.setActionCommand("addMarcaBtn");
        vistaUsuario.eliminarMarcaBtn.addActionListener(listener);
        vistaUsuario.eliminarMarcaBtn.setActionCommand("eliminarMarcaBtn");
        vistaUsuario.modificarMarcaBtn.addActionListener(listener);
        vistaUsuario.modificarMarcaBtn.setActionCommand("modificarMarcaBtn");
        vistaUsuario.btnListadoMarcas.addActionListener(listener);
        vistaUsuario.btnListadoMarcas.setActionCommand("btnListadoMarcas");

        //Listener de propietarios
        vistaUsuario.addPropietarioBtn.addActionListener(listener);
        vistaUsuario.addPropietarioBtn.setActionCommand("addPropietarioBtn");
        vistaUsuario.modificarPropietarioBtn.addActionListener(listener);
        vistaUsuario.modificarPropietarioBtn.setActionCommand("modificarPropietarioBtn");
        vistaUsuario.eliminarPropietarioBtn.addActionListener(listener);
        vistaUsuario.eliminarPropietarioBtn.setActionCommand("eliminarPropietarioBtn");
        vistaUsuario.instrumentosBtn.addActionListener(listener);
        vistaUsuario.instrumentosBtn.setActionCommand("instrumentosBtn");
        vistaUsuario.btnlistadoPropietarios.addActionListener(listener);
        vistaUsuario.btnlistadoPropietarios.setActionCommand("btnlistadoPropietarios");

        //Listener de Fabricación
        vistaUsuario.insertarFabBtn.addActionListener(listener);
        vistaUsuario.insertarFabBtn.setActionCommand("insertarFabBtn");
        vistaUsuario.modificarFabBtn.addActionListener(listener);
        vistaUsuario.modificarFabBtn.setActionCommand("modificarFabBtn");
        vistaUsuario.borrarFabBtn.addActionListener(listener);
        vistaUsuario.borrarFabBtn.setActionCommand("borrarFabBtn");
        vistaUsuario.btnListadoFabricados.addActionListener(listener);
        vistaUsuario.btnListadoFabricados.setActionCommand("btnListadoFabricados");

        //Listener menú
        vistaUsuario.conexionItem.addActionListener(listener);
        vistaUsuario.itemSalir.addActionListener(listener);


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        modelo = new Modelo();
        String comando = e.getActionCommand();

        switch (comando) {

            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;

            case "Conectar":
                vistaUsuario.conexionItem.setEnabled(false);
                modelo.conectar();
                break;

            case "addInstrumentobtn":
                Instrumento nuevoInstrumento = new Instrumento();
                nuevoInstrumento.setCodigo(vistaUsuario.txtCodigo.getText());
                nuevoInstrumento.setTipo(String.valueOf(vistaUsuario.comboTipo.getSelectedItem()));
                nuevoInstrumento.setNombre(vistaUsuario.txtNombre.getText());
                try {
                    nuevoInstrumento.setPvp(Double.parseDouble(vistaUsuario.txtPvp.getText()));
                } catch (NumberFormatException ex) {
                    util.showErrorAlert("Introduce números en precio");
                }
                nuevoInstrumento.setFechaAdquisicion(Date.valueOf(vistaUsuario.datePicker.getDate()));
                nuevoInstrumento.setPropietario((Propietario) vistaUsuario.comboPropietarios.getSelectedItem());

                modelo.insertar(nuevoInstrumento);
                limpiarCamposInstrumentos();
                listarInstrumentos(modelo.getInstrumentos());

                break;

            case "modificarInstrumentosBtn":
                Instrumento instrumentoSeleccionado = (Instrumento) vistaUsuario.listaInstrumentos.getSelectedValue();
                instrumentoSeleccionado.setCodigo(vistaUsuario.txtCodigo.getText());
                instrumentoSeleccionado.setTipo(String.valueOf(vistaUsuario.comboTipo.getSelectedItem()));
                instrumentoSeleccionado.setNombre(vistaUsuario.txtNombre.getText());
                try {
                    instrumentoSeleccionado.setPvp(Double.parseDouble(vistaUsuario.txtPvp.getText()));
                } catch (NumberFormatException e1) {
                    util.showErrorAlert("Introduce números en precio");
                }
                instrumentoSeleccionado.setFechaAdquisicion(Date.valueOf(vistaUsuario.datePicker.getDate()));
                instrumentoSeleccionado.setPropietario((Propietario) vistaUsuario.comboPropietarios.getSelectedItem());
                modelo.modificar(instrumentoSeleccionado);
                limpiarCamposInstrumentos();
                listarInstrumentos(modelo.getInstrumentos());

                break;

            case "eliminarInstrumentoBtn":
                Instrumento instrumentoBorrado = (Instrumento) vistaUsuario.listaInstrumentos.getSelectedValue();
                modelo.eliminar(instrumentoBorrado);
                limpiarCamposInstrumentos();
                listarInstrumentos(modelo.getInstrumentos());

                break;

            case "btnlistadoInstrumentos":
                listarInstrumentos(modelo.getInstrumentos());
                break;

            case "addMarcaBtn":
                Marca nuevaMarca = new Marca();
                nuevaMarca.setNombremarca(vistaUsuario.txtNombreMarca.getText());
                nuevaMarca.setPais(String.valueOf(vistaUsuario.comboPais.getSelectedItem()));
                nuevaMarca.setDelegacion(vistaUsuario.txtDelegacion.getText());
                nuevaMarca.setWeb(vistaUsuario.txtWeb.getText());
                modelo.insertar(nuevaMarca);
                limpiarCamposMarcas();
                listarMarcas(modelo.getMarcas());

                break;

            case "modificarMarcaBtn":
                Marca marcaSeleccionada = (Marca) vistaUsuario.listaMarcas.getSelectedValue();
                marcaSeleccionada.setNombremarca(vistaUsuario.txtNombreMarca.getText());
                marcaSeleccionada.setPais(String.valueOf(vistaUsuario.comboPais.getSelectedItem()));
                marcaSeleccionada.setDelegacion(vistaUsuario.txtDelegacion.getText());
                marcaSeleccionada.setWeb(vistaUsuario.txtWeb.getText());
                modelo.modificar(marcaSeleccionada);
                limpiarCamposMarcas();
                listarMarcas(modelo.getMarcas());
                break;

            case "eliminarMarcaBtn":
                Marca marcaBorrada = (Marca) vistaUsuario.listaMarcas.getSelectedValue();
                modelo.eliminar(marcaBorrada);
                limpiarCamposMarcas();
                listarMarcas(modelo.getMarcas());
                break;

            case "btnListadoMarcas":
                listarMarcas(modelo.getMarcas());
                break;

            case "addMaterialBtn":
                Material nuevoMaterial = new Material();
                nuevoMaterial.setNombreMaterial(vistaUsuario.txtNombreMaterial.getText());
                nuevoMaterial.setCalidad(String.valueOf(vistaUsuario.comboCalidad.getSelectedItem()));
                if (vistaUsuario.rbtnNo.isSelected()) {
                    nuevoMaterial.setOrganico("No");
                }
                if (vistaUsuario.rbtnSi.isSelected()) {
                    nuevoMaterial.setOrganico("Si");
                }
                nuevoMaterial.setProcedencia(vistaUsuario.txtProcedencia.getText());
                modelo.insertar(nuevoMaterial);
                limpiarCamposMateriales();
                listarMateriales(modelo.getMateriales());
                break;

            case "modificarMaterialBtn":
                Material materialSeleccionado = (Material) vistaUsuario.listaMateriales.getSelectedValue();
                materialSeleccionado.setNombreMaterial(vistaUsuario.txtNombreMaterial.getText());
                materialSeleccionado.setCalidad(String.valueOf(vistaUsuario.comboCalidad.getSelectedItem()));
                if (vistaUsuario.rbtnNo.isSelected()) {
                    materialSeleccionado.setOrganico("No");
                }
                if (vistaUsuario.rbtnSi.isSelected()) {
                    materialSeleccionado.setOrganico("Si");
                }
                materialSeleccionado.setProcedencia(vistaUsuario.txtProcedencia.getText());
                modelo.modificar(materialSeleccionado);
                limpiarCamposMateriales();
                listarMateriales(modelo.getMateriales());
                break;

            case "eliminarMaterialBtn":
                Material materialBorrado = (Material) vistaUsuario.listaMateriales.getSelectedValue();
                modelo.eliminar(materialBorrado);
                limpiarCamposMarcas();
                listarMateriales(modelo.getMateriales());
                break;

            case "btnlistadoMateriales":
                listarMateriales(modelo.getMateriales());
                break;

            case "addPropietarioBtn":
                Propietario nuevoPropietario = new Propietario();
                nuevoPropietario.setNombre(vistaUsuario.txtNombrePropietario.getText());
                nuevoPropietario.setApellidos(vistaUsuario.txtApellidos.getText());
                nuevoPropietario.setDccion(vistaUsuario.txtDccion.getText());
                nuevoPropietario.setTfno(Integer.parseInt(vistaUsuario.txtTfno.getText()));
                modelo.insertar(nuevoPropietario);
                limpiarCamposPropietarios();
                listarPropietarios(modelo.getPropietarios());
                break;

            case "modificarPropietarioBtn":
                Propietario propietarioSeleccionado = (Propietario) vistaUsuario.listaPropietarios.getSelectedValue();
                propietarioSeleccionado.setNombre(vistaUsuario.txtNombrePropietario.getText());
                propietarioSeleccionado.setApellidos(vistaUsuario.txtApellidos.getText());
                propietarioSeleccionado.setDccion(vistaUsuario.txtDccion.getText());
                propietarioSeleccionado.setTfno(Integer.parseInt(vistaUsuario.txtTfno.getText()));
                modelo.modificar(propietarioSeleccionado);
                limpiarCamposPropietarios();
                listarPropietarios(modelo.getPropietarios());

                break;

            case "eliminarPropietarioBtn":
                Propietario borrarPropietario = (Propietario) vistaUsuario.listaPropietarios.getSelectedValue();
                modelo.eliminar(borrarPropietario);
                limpiarCamposPropietarios();
                listarPropietarios(modelo.getPropietarios());
                break;

            case "btnlistadoPropietarios":
                listarPropietarios(modelo.getPropietarios());
                break;

            case "insertarFabBtn":
                Instrumentomateriales nuevoFabricado = new Instrumentomateriales();
                try {
                    nuevoFabricado.setCantidad(Integer.parseInt(vistaUsuario.txtCantidad.getText()));
                } catch (NumberFormatException ex) {
                    util.showErrorAlert("Introduce números en precio");
                }
                nuevoFabricado.setInstrumento((Instrumento) vistaUsuario.comboInstrumentos.getSelectedItem());
                nuevoFabricado.setMaterial((Material) vistaUsuario.comboMaterial.getSelectedItem());
                modelo.insertar(nuevoFabricado);
                limpiarCamposFabricados();
                listarFabricados(modelo.getFabricaciones());

                break;

            case "modificarFabBtn":
                Instrumentomateriales fabricadoSeleccionado = (Instrumentomateriales) vistaUsuario.listaFabricados.getSelectedValue();
                try {
                    fabricadoSeleccionado.setCantidad(Integer.parseInt(vistaUsuario.txtCantidad.getText()));
                } catch (NumberFormatException ex) {
                    util.showErrorAlert("Introduce números en precio");
                }
                fabricadoSeleccionado.setInstrumento((Instrumento) vistaUsuario.comboInstrumentos.getSelectedItem());
                fabricadoSeleccionado.setMaterial((Material) vistaUsuario.comboMaterial.getSelectedItem());
                modelo.modificar(fabricadoSeleccionado);
                limpiarCamposFabricados();
                listarFabricados(modelo.getFabricaciones());
                break;

            case "borrarFabBtn":
                Instrumentomateriales borrarFabricado = (Instrumentomateriales) vistaUsuario.listaFabricados.getSelectedValue();
                modelo.eliminar(borrarFabricado);
                limpiarCamposFabricados();
                listarFabricados(modelo.getFabricaciones());
                break;

            case "btnListadoFabricados":
                listarFabricados(modelo.getFabricaciones());
                break;

            case "instrumentosBtn":
                Propietario prop= (Propietario) vistaUsuario.listaPropietarios.getSelectedValue();
                prop.setInstrumento((ArrayList)modelo.getPropietariosInstrumento(prop));
                listarPropietarioInstrumentos(modelo.getPropietariosInstrumento(prop));
                break;

        }
        limpiarTodo();
        listarTodo();
    }

    private void limpiarTodo(){
        limpiarCamposPropietarios();
        limpiarCamposMarcas();
        limpiarCamposInstrumentos();
        limpiarCamposFabricados();
        limpiarCamposMateriales();
    }

    private void limpiarCamposFabricados() {
        vistaUsuario.txtCantidad.setText("");
        vistaUsuario.comboInstrumentos.setSelectedIndex(-1);
        vistaUsuario.comboMaterial.setSelectedIndex(-1);

    }

    private void limpiarCamposPropietarios() {
        vistaUsuario.txtNombrePropietario.setText("");
        vistaUsuario.txtApellidos.setText("");
        vistaUsuario.txtDccion.setText("");
        vistaUsuario.txtTfno.setText("");
    }

    private void limpiarCamposMateriales() {
        vistaUsuario.txtNombreMaterial.setText("");
        vistaUsuario.comboCalidad.setSelectedIndex(-1);
        vistaUsuario.rbtnSi.setSelected(false);
        vistaUsuario.rbtnNo.setSelected(false);
        vistaUsuario.txtProcedencia.setText("");
    }

    private void limpiarCamposMarcas() {
        vistaUsuario.txtNombreMarca.setText("");
        vistaUsuario.comboPais.setSelectedIndex(-1);
        vistaUsuario.txtDelegacion.setText("");
        vistaUsuario.txtWeb.setText("");
    }

    private void limpiarCamposInstrumentos() {
        vistaUsuario.txtCodigo.setText("");
        vistaUsuario.comboTipo.setSelectedIndex(-1);
        vistaUsuario.txtNombre.setText("");
        vistaUsuario.txtPvp.setText("");
        vistaUsuario.datePicker.setText("");
        vistaUsuario.comboPropietarios.setSelectedIndex(-1);
    }

    private void listarTodo(){
        listarInstrumentos(modelo.getInstrumentos());
        listarMateriales(modelo.getMateriales());
        listarMarcas(modelo.getMarcas());
        listarFabricados(modelo.getFabricaciones());
        listarPropietarios(modelo.getPropietarios());

    }

    private void listarInstrumentos(ArrayList<Instrumento> instrumentos) {
        vistaUsuario.dlmInstrumentos.clear();
        for (Instrumento instrumento : instrumentos) {
            vistaUsuario.dlmInstrumentos.addElement(instrumento);
        }

        vistaUsuario.comboInstrumentos.removeAllItems();
        ArrayList<Instrumento> ins=modelo.getInstrumentos();
        for(Instrumento i: ins) {
            vistaUsuario.comboInstrumentos.addItem(i);
        }
        vistaUsuario.comboInstrumentos.setSelectedItem(-1);

    }

    public void listarMarcas(ArrayList<Marca> marcas) {
        vistaUsuario.dlmMarca.clear();
        for (Marca marca : marcas) {
            vistaUsuario.dlmMarca.addElement(marca);
        }
    }

    public void listarMateriales(ArrayList<Material> materiales) {
        vistaUsuario.dlmMaterial.clear();
        for (Material material : materiales) {
            vistaUsuario.dlmMaterial.addElement(material);
        }

        vistaUsuario.comboMaterial.removeAllItems();
        ArrayList<Material> mat=modelo.getMateriales();
        for(Material m: mat) {
            vistaUsuario.comboInstrumentos.addItem(m);
        }
        vistaUsuario.comboMaterial.setSelectedItem(-1);
    }

    public void listarPropietarios(ArrayList<Propietario> propietarios) {
        vistaUsuario.dlmPropietario.clear();
        for (Propietario propietario : propietarios) {
            vistaUsuario.dlmPropietario.addElement(propietario);
        }

        vistaUsuario.comboPropietarios.removeAllItems();
        ArrayList<Propietario> prop=modelo.getPropietarios();
        for (Propietario pr:prop){
            vistaUsuario.comboPropietarios.addItem(pr);
        }
        vistaUsuario.comboPropietarios.setSelectedItem(-1);
    }

    public void listarFabricados(ArrayList<Instrumentomateriales> fabricados) {
        vistaUsuario.dlmFabricaciones.clear();
        for (Instrumentomateriales fabricado : fabricados) {
            vistaUsuario.dlmFabricaciones.addElement(fabricado);
        }

        vistaUsuario.comboInstrumentos.removeAllItems();
        ArrayList<Instrumento> ins=modelo.getInstrumentos();
        vistaUsuario.comboMaterial.removeAllItems();
        ArrayList<Material> mat=modelo.getMateriales();
        for (Instrumento i: ins) {
            vistaUsuario.comboInstrumentos.addItem(i);
        }
        for (Material m:mat) {
            vistaUsuario.comboMaterial.addItem(m);
        }

        vistaUsuario.comboInstrumentos.setSelectedItem(-1);
        vistaUsuario.comboMaterial.setSelectedItem(-1);
    }

    public void listarPropietarioInstrumentos(List <Instrumento> lista){
        vistaUsuario.dlmInstrumentosPropietario.clear();
        for (Instrumento instrumento: lista) {
            vistaUsuario.dlmInstrumentosPropietario.addElement(instrumento);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vistaUsuario.listaInstrumentos) {
                Instrumento instrumento = (Instrumento) vistaUsuario.listaInstrumentos.getSelectedValue();
                vistaUsuario.txtCodigo.setText(String.valueOf(instrumento.getCodigo()));
                vistaUsuario.comboTipo.setSelectedItem(instrumento.getTipo());
                vistaUsuario.txtNombre.setText(String.valueOf(instrumento.getNombre()));
                vistaUsuario.txtPvp.setText(String.valueOf(instrumento.getPvp()));
                vistaUsuario.datePicker.setDate(instrumento.getFechaAdquisicion().toLocalDate());
                vistaUsuario.comboPropietarios.setSelectedItem(instrumento.getPropietario());
            }else if(e.getSource()== vistaUsuario.listaMarcas){
                Marca marca= (Marca) vistaUsuario.listaMarcas.getSelectedValue();
                vistaUsuario.txtNombreMarca.setText(String.valueOf(marca.getNombremarca()));
                vistaUsuario.comboPais.setSelectedItem(marca.getPais());
                vistaUsuario.txtDelegacion.setText(String.valueOf(marca.getDelegacion()));
                vistaUsuario.txtWeb.setText(String.valueOf(marca.getWeb()));
            }else if(e.getSource()== vistaUsuario.listaMateriales) {
                Material material= (Material) vistaUsuario.listaMateriales.getSelectedValue();
                vistaUsuario.txtNombreMaterial.setText(String.valueOf(material.getNombreMaterial()));
                vistaUsuario.comboCalidad.setSelectedItem(material.getCalidad());
                if (vistaUsuario.rbtnNo.isSelected()) vistaUsuario.rbtnNo.setText("No");
                if (vistaUsuario.rbtnSi.isSelected()) vistaUsuario.rbtnSi.setText("Si");
                vistaUsuario.txtProcedencia.setText(String.valueOf(material.getProcedencia()));
            }else if(e.getSource()== vistaUsuario.listaPropietarios){
                Propietario propietario= (Propietario) vistaUsuario.listaPropietarios.getSelectedValue();
                vistaUsuario.txtNombrePropietario.setText(String.valueOf(propietario.getNombre()));
                vistaUsuario.txtApellidos.setText(String.valueOf(propietario.getApellidos()));
                vistaUsuario.txtDccion.setText(propietario.getDccion());
                vistaUsuario.txtTfno.setText(String.valueOf(propietario.getTfno()));
            }else if(e.getSource()== vistaUsuario.listaFabricados){
                Instrumentomateriales fabricado= (Instrumentomateriales) vistaUsuario.listaFabricados.getSelectedValue();
                vistaUsuario.txtCantidad.setText(String.valueOf(fabricado.getCantidad()));
                vistaUsuario.comboInstrumentos.setSelectedItem(fabricado.getInstrumento());
                vistaUsuario.comboMaterial.setSelectedItem(fabricado.getMaterial());
            }
        }
    }
}

