package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.TipoPlataformas;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;

/**
 * Clase encargada de mostrar y fabricar la interfaz del los usuarios propios del departamento de tráfico de una
 * empresa de transporte.
 */
public class VistaUsuario extends JFrame {

    /**
     * Elementos y atributos que intervienen en la interfaz que se declaran
     */

    private JTabbedPane pestanas = new JTabbedPane();

    //Pestaña Chóferes
    private JPanel panelChoferes;
    JLabel lblBuscarChofer;

    JTextField txtBuscarChofer;
    JScrollPane scrollChoferes;
    JTable tablaChoferes;
    DefaultTableModel dtmChoferes;
    JButton btnListChofer;

    //Pestaña Clientes
    private JPanel panelClientes;
    JLabel lblNombreCliente;
    JLabel lblCifCliente;
    JLabel lblDccionCliente;
    JLabel lblCPCliente;
    JLabel lblPoblacionCliente;
    JLabel lblProvinciaCliente;
    JLabel lblTfnoCliente;
    JLabel lblBuscarCliente;
    JTextField txtNombreCliente;
    JTextField txtCifCliente;
    JTextField txtDccionCliente;
    JTextField txtCPCliente;
    JTextField txtPoblacionCliente;
    JTextField txtProvinciaCliente;
    JTextField txtTfnoCliente;
    JTextField txtBuscarCliente;
    JScrollPane scrollClientes;
    JTable tablaClientes;
    DefaultTableModel dtmClientes;
    JButton btnAddCliente;
    JButton btnDelCliente;
    JButton btnModCliente;
    JButton btnListCliente;

    //Pestaña Proveedores
    private JPanel panelProveedores;
    JLabel lblNombreProveedor;
    JLabel lblCifProveedor;
    JLabel lblDccionProveedor;
    JLabel lblCPProveedor;
    JLabel lblPoblacionProveedor;
    JLabel lblProvinciaProveedor;
    JLabel lblTfnoProveedor;
    JLabel lblBuscarProveedor;

    JTextField txtNombreProveedor;
    JTextField txtCifProveedor;
    JTextField txtDccionProveedor;
    JTextField txtCPProveedor;
    JTextField txtPoblacionProveedor;
    JTextField txtProvinciaProveedor;
    JTextField txtTfnoProveedor;
    JTextField txtBuscarProveedor;
    JScrollPane scrollProveedores;
    JTable tablaProveedores;
    DefaultTableModel dtmProveedores;
    JButton btnAddProveedor;
    JButton btnDelProveedor;
    JButton btnModProveedor;
    JButton btnListProveedor;

    //Pestaña Semirremolques
    private JPanel panelSemirremolques;
    JLabel lblMatriculaSemi;
    JLabel lblITVSemi;
    JLabel lblTipoSemi;
    JLabel lblSeguroSemi;
    JLabel lblBuscarSemi;

    JTextField txtMatriculaSemi;
    DatePicker fechaITVSemi;
    JComboBox comboTipoSemi;
    DatePicker fechaSeguroSemi;
    JTextField txtBuscarSemi;
    JScrollPane scrollSemirremolques;
    JTable tablaSemirremolques;
    DefaultTableModel dtmSemirremolques;
    JButton btnAddSemi;
    JButton btnDelSemi;
    JButton btnModSemi;
    JButton btnListSemi;

    //Pestaña Tractoras
    private JPanel panelTractoras;
    JLabel lblMatriculaTractora;
    JLabel lblITVTractora;
    JLabel lblLimitador;
    JLabel lblTacografo;
    JLabel lblSeguroTractora;
    JLabel lblTarjetaTte;
    JLabel lblBuscarTractora;

    JTextField txtMatriculaTractora;
    DatePicker fechaITVTractora;
    DatePicker fechaLimitador;
    DatePicker fechaTacografo;
    DatePicker fechaSeguroTractora;
    JTextField txtTarjetaTte;
    JTextField txtBuscarTractora;
    JScrollPane scrollTractoras;
    JTable tablaTractoras;
    DefaultTableModel dtmTractoras;
    JButton btnAddTractora;
    JButton btnDelTractora;
    JButton btnModTractora;
    JButton btnListTractora;

    //Pestaña Viajes
    private JPanel panelViajes;
    JLabel lblMatriculaViaje;
    JLabel lblSemirremolque;
    JLabel lblClienteViaje;
    JLabel lblChoferViaje;
    JLabel lblOrigen;
    JLabel lblDestino;
    JLabel lblPrecio;
    JLabel lblPeso;
    JLabel lblFechaViaje;
    JLabel lblAlbaran;
    JLabel lblBuscarViaje;

    JComboBox comboMatricula;
    JComboBox comboSemi;
    JComboBox comboCliente;
    DatePicker fechaViaje;
    JComboBox comboChofer;
    JTextField txtPrecio;
    JTextField txtOrigen;
    JTextField txtDestino;
    JTextField txtAlbaran;
    JTextField txtPeso;
    JTextField txtBuscarViaje;
    JScrollPane scrollViajes;
    JTable tablaViajes;
    DefaultTableModel dtmViajes;
    JButton btnAddViaje;
    JButton btnDelViaje;
    JButton btnModViaje;
    JButton btnListViaje;

    //Pestaña vendidos
    private JPanel panelVendidos;
    JLabel lblProveedorViaje;
    JLabel lblMatriculaProveedor;
    JLabel lblSemiProveedor;
    JLabel lblAlbaranProveedor;
    JLabel lblFechaViajeProveedor;
    JLabel lblPrecioProveedor;
    JLabel lblOrigenProveedor;
    JLabel lblDestinoProveedor;
    JLabel lblPesoProveedor;
    JLabel lblClienteProveedor;
    JLabel lblBuscarViajeProveedor;

    JComboBox comboProveedor;
    JComboBox comboClienteProveedor;
    JTextField txtMatriculaViajeProveedor;
    JTextField txtMatriculaSemiViajeProveedor;
    DatePicker fechaViajeProveedor;
    JTextField txtPrecioProveedor;
    JTextField txtOrigenProveedor;
    JTextField txtDestinoProveedor;
    JTextField txtPesoProveedor;
    JTextField txtAlbaranProveedor;
    JTextField txtBuscarViajeProveedor;

    JButton btnAddViajeProveedor;
    JButton btnModViajeProveedor;
    JButton btnDelViajeProveedor;
    JButton btnListViajeProveedor;

    JScrollPane scrollVendidos;
    JTable tablaVendidos;
    DefaultTableModel dtmVendidos;

    //MENU
    JMenu menuArchivo;
    JMenuItem itemSalir;
    JMenuItem itemOpciones;
    JMenuItem itemUsuarios;

    /**
     * Constructor donde se inicia la ventana del interfaz
     */

    public VistaUsuario() {
        setTitle("TRANSIT DIGITAL");
        this.setSize(1024, 860);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        inicioPanel();
    }

    /**
     * Método que inicia los paneles, que se identifican como pestañas
     */

    private void inicioPanel() {
        pestanas = new JTabbedPane();
        panelChoferes = new JPanel();
        inicioPestanas(panelChoferes, "Chóferes");
        panelClientes = new JPanel();
        inicioPestanas(panelClientes, "Clientes");
        panelProveedores = new JPanel();
        inicioPestanas(panelProveedores, "Proveedores");
        panelSemirremolques = new JPanel();
        inicioPestanas(panelSemirremolques, "Semirremolques");
        panelTractoras = new JPanel();
        inicioPestanas(panelTractoras, "Tractoras");
        panelViajes = new JPanel();
        inicioPestanas(panelViajes, "Viajes");
        panelVendidos = new JPanel();
        inicioPestanas(panelVendidos, "Vendidos");
        inicioElementos();
        pestanas.setBackground(new Color(190, 177, 35));
        pestanas.setFont(new Font("Sans Serif", Font.BOLD, 24));
        pestanas.setForeground(Color.WHITE);
        add(pestanas);

    }

    /**
     * Método que realiza la llamada de los distintos métodos que inician los elementos divididos por su naturaleza
     */

    private void inicioElementos() {
        etiquetas();
        campos();
        botones();
        tablas();
        crearMenu();
        setEnumCombo();
        logoVentana();
    }

    /**
     * Método dedicado a la automatización de pintar el logo de la empresa de software en todas las pestañas
     */

    private void logoVentana() {
        JLabel lblLogo[] = new JLabel[7];
        lblLogo[0] = new JLabel();
        lblLogo[1] = new JLabel();
        lblLogo[2] = new JLabel();
        lblLogo[3] = new JLabel();
        lblLogo[4] = new JLabel();
        lblLogo[5] = new JLabel();
        lblLogo[6] = new JLabel();

        for (JLabel logo : lblLogo) {
            logo.setBounds(250, 0, 508, 122);
            logo.setIcon(new ImageIcon("img/logoventana.png"));
        }

        panelChoferes.add(lblLogo[0]);
        panelClientes.add(lblLogo[1]);
        panelProveedores.add(lblLogo[2]);
        panelSemirremolques.add(lblLogo[3]);
        panelTractoras.add(lblLogo[4]);
        panelViajes.add(lblLogo[5]);
        panelVendidos.add(lblLogo[6]);
    }

    /**
     * Método que automatiza los elementos comunes de toas las pestañas
     * @param panel es el panel parametrizado
     * @param nombrePestana es la pestaña parametrizada
     */

    private void inicioPestanas(JPanel panel, String nombrePestana) {
        panel.setLayout(null);
        panel.setBackground(new Color(254, 242, 113));
        this.getContentPane().add(panel);
        pestanas.add(nombrePestana, panel);
    }

    /**
     * Método que inicia las etiquetas, que son aquellos elementos fijos e inamovibles que no requieren ningún tipo de
     * interacción por parte del usuario de cada panel
     */

    private void etiquetas() {
        etiquetasChoferes();
        etiquetasClientes();
        etiquetasProvedores();
        etiquetasSemirremolques();
        etiquetasTractoras();
        etiquetasViajes();
        etiquetasVendidos();
    }

    /**
     * Método que inicia los campos, que son aquellos elementos que precisan la intervención del usuario porque deben
     * ser rellenados por él mismo en cada panel
     */

    private void campos() {
        camposChoferes();
        camposClientes();
        camposProveedores();
        camposSemirremolques();
        camposTractoras();
        camposViajes();
        camposViajesVendidos();
    }

    /**
     * Método para iniciar las tablas de cada tipo de tabla de la BBDD
     */

    private void tablas(){
        iniciarTablaChoferes();
        iniciarTablaClientes();
        iniciarTablaProveedores();
        iniciarTablaSemirremolques();
        iniciarTablaTractoras();
        iniciarTablaViajes();
        iniciarTablaVendidos();
    }

    /**
     * Método encargado de colocar cada botón de la interfaz
     */

    private void botones() {
        btnListChofer = new JButton();
        btnListChofer.setBounds(20, 625, 100, 66);
        btnListChofer.setIcon(new ImageIcon("img/botonlistar.png"));
        btnListChofer.setOpaque(false);
        btnListChofer.setContentAreaFilled(false);
        btnListChofer.setBorderPainted(false);
        panelChoferes.add(btnListChofer);

        btnAddCliente = new JButton();
        btnModCliente = new JButton();
        btnDelCliente = new JButton();
        btnListCliente= new JButton();
        formatoBotones(btnAddCliente, btnModCliente, btnDelCliente, btnListCliente, panelClientes);

        btnAddProveedor = new JButton();
        btnModProveedor = new JButton();
        btnDelProveedor = new JButton();
        btnListProveedor= new JButton();
        formatoBotones(btnAddProveedor, btnModProveedor, btnDelProveedor,btnListProveedor, panelProveedores);

        btnAddSemi = new JButton();
        btnModSemi = new JButton();
        btnDelSemi = new JButton();
        btnListSemi= new JButton();
        formatoBotones(btnAddSemi, btnModSemi, btnDelSemi,btnListSemi, panelSemirremolques);

        btnAddTractora = new JButton();
        btnModTractora = new JButton();
        btnDelTractora = new JButton();
        btnListTractora= new JButton();
        formatoBotones(btnAddTractora, btnAddTractora, btnAddTractora, btnListTractora, panelTractoras);

        btnAddViaje = new JButton();
        btnModViaje = new JButton();
        btnDelViaje = new JButton();
        btnListViaje= new JButton();
        formatoBotones(btnAddViaje, btnModViaje, btnDelViaje, btnListViaje, panelViajes);

        btnAddViajeProveedor = new JButton();
        btnModViajeProveedor = new JButton();
        btnDelViajeProveedor = new JButton();
        btnListViajeProveedor= new JButton();
        formatoBotones(btnAddViajeProveedor, btnModViajeProveedor, btnDelViajeProveedor,btnListViajeProveedor, panelVendidos);

    }

    /**
     * Método que pinta las etiquetas del panel de chóferes
     */

    private void etiquetasChoferes() {
        lblBuscarChofer = new JLabel();
        lblBuscarChofer.setBounds(590, 264, 50, 50);
        lblBuscarChofer.setIcon(new ImageIcon("img/lupa.png"));
        panelChoferes.add(lblBuscarChofer);
    }

    /**
     * Método que pinta los campos del panel de chóferes
     */

    private void camposChoferes() {
        txtBuscarChofer = new JTextField();
        txtBuscarChofer.setBounds(635, 275, 330, 25);
        formatoTextoSinCursorDerecha(txtBuscarChofer, panelChoferes);

        scrollChoferes = new JScrollPane();
        scrollChoferes.setBounds(135, 340, 830, 375);
        panelChoferes.add(scrollChoferes);

        tablaChoferes = new JTable();
        scrollChoferes.setViewportView(tablaChoferes);
        tablaChoferes.setBackground(Color.WHITE);
    }

    /**
     * Método que pinta las etiquetas de los clientes
     */

    private void etiquetasClientes() {
        lblNombreCliente = new JLabel("Nombre");
        lblNombreCliente.setBounds(25, 125, 74, 25);
        formatoEtiquetas(lblNombreCliente, panelClientes);

        lblCifCliente = new JLabel("CIF");
        lblCifCliente.setBounds(550, 125, 50, 25);
        formatoEtiquetas(lblCifCliente, panelClientes);

        lblTfnoCliente = new JLabel();
        lblTfnoCliente.setBounds(775, 112, 50, 50);
        lblTfnoCliente.setIcon(new ImageIcon("img/telefono.png"));
        panelClientes.add(lblTfnoCliente);

        lblDccionCliente = new JLabel("Dirección");
        lblDccionCliente.setBounds(25, 175, 100, 25);
        formatoEtiquetas(lblDccionCliente, panelClientes);

        lblCPCliente = new JLabel("CP");
        lblCPCliente.setBounds(25, 225, 50, 25);
        formatoEtiquetas(lblCPCliente, panelClientes);

        lblPoblacionCliente = new JLabel("Población");
        lblPoblacionCliente.setBounds(205, 225, 100, 25);
        formatoEtiquetas(lblPoblacionCliente, panelClientes);

        lblProvinciaCliente = new JLabel("Provincia");
        lblProvinciaCliente.setBounds(600, 225, 100, 25);
        formatoEtiquetas(lblProvinciaCliente, panelClientes);

        lblBuscarCliente = new JLabel();
        lblBuscarCliente.setBounds(25, 264, 50, 50);
        lblBuscarCliente.setIcon(new ImageIcon("img/lupa.png"));
        panelClientes.add(lblBuscarCliente);
    }

    /**
     * Método que pinta los campos de los clientes
     */

    private void camposClientes() {
        txtNombreCliente = new JTextField();
        txtNombreCliente.setBounds(120, 125, 400, 25);
        formatoTextoSinCursorDerecha(txtNombreCliente, panelClientes);

        txtCifCliente = new JTextField();
        txtCifCliente.setBounds(600, 125, 150, 25);
        formatoTexto(txtCifCliente, panelClientes);

        txtTfnoCliente = new JTextField();
        txtTfnoCliente.setBounds(815, 125, 150, 25);
        formatoTexto(txtTfnoCliente, panelClientes);

        txtDccionCliente = new JTextField();
        txtDccionCliente.setBounds(140, 175, 825, 25);
        formatoTextoSinCursorDerecha(txtDccionCliente, panelClientes);

        txtCPCliente = new JTextField();
        txtCPCliente.setBounds(75, 225, 100, 25);
        formatoTexto(txtCPCliente, panelClientes);

        txtPoblacionCliente = new JTextField();
        txtPoblacionCliente.setBounds(320, 225, 255, 25);
        formatoTextoSinCursorDerecha(txtPoblacionCliente, panelClientes);

        txtProvinciaCliente = new JTextField();
        txtProvinciaCliente.setBounds(710, 225, 255, 25);
        formatoTextoSinCursorDerecha(txtProvinciaCliente, panelClientes);

        txtBuscarCliente = new JTextField();
        txtBuscarCliente.setBounds(75, 275, 330, 25);
        formatoTextoSinCursorDerecha(txtBuscarCliente, panelClientes);

        scrollClientes = new JScrollPane();
        scrollClientes.setBounds(135, 340, 830, 375);
        panelClientes.add(scrollClientes);

        tablaClientes = new JTable();
        scrollClientes.setViewportView(tablaClientes);
        tablaClientes.setBackground(Color.WHITE);
    }

    /**
     * Método que pinta las etiquetas del panel de proveedores
     */

    private void etiquetasProvedores() {
        lblNombreProveedor = new JLabel("Nombre");
        lblNombreProveedor.setBounds(25, 125, 74, 25);
        formatoEtiquetas(lblNombreProveedor, panelProveedores);

        lblCifProveedor = new JLabel("CIF");
        lblCifProveedor.setBounds(550, 125, 50, 25);
        formatoEtiquetas(lblCifProveedor, panelProveedores);

        lblTfnoProveedor = new JLabel();
        lblTfnoProveedor.setBounds(775, 112, 50, 50);
        lblTfnoProveedor.setIcon(new ImageIcon("img/telefono.png"));
        panelProveedores.add(lblTfnoProveedor);

        lblDccionProveedor = new JLabel("Dirección");
        lblDccionProveedor.setBounds(25, 175, 100, 25);
        formatoEtiquetas(lblDccionProveedor, panelProveedores);

        lblCPProveedor = new JLabel("CP");
        lblCPProveedor.setBounds(25, 225, 50, 25);
        formatoEtiquetas(lblCPProveedor, panelProveedores);

        lblPoblacionProveedor = new JLabel("Población");
        lblPoblacionProveedor.setBounds(205, 225, 100, 25);
        formatoEtiquetas(lblPoblacionProveedor, panelProveedores);

        lblProvinciaProveedor = new JLabel("Provincia");
        lblProvinciaProveedor.setBounds(600, 225, 100, 25);
        formatoEtiquetas(lblProvinciaProveedor, panelProveedores);

        lblBuscarProveedor = new JLabel();
        lblBuscarProveedor.setBounds(25, 264, 50, 50);
        lblBuscarProveedor.setIcon(new ImageIcon("img/lupa.png"));
        panelProveedores.add(lblBuscarProveedor);
    }

    /**
     * Método que pinta los campos del panel de proveedores
     */

    private void camposProveedores() {
        txtNombreProveedor = new JTextField();
        txtNombreProveedor.setBounds(120, 125, 400, 25);
        formatoTextoSinCursorDerecha(txtNombreProveedor, panelProveedores);

        txtCifProveedor = new JTextField();
        txtCifProveedor.setBounds(600, 125, 150, 25);
        formatoTexto(txtCifProveedor, panelProveedores);

        txtTfnoProveedor = new JTextField();
        txtTfnoProveedor.setBounds(815, 125, 150, 25);
        formatoTexto(txtTfnoProveedor, panelProveedores);

        txtDccionProveedor = new JTextField();
        txtDccionProveedor.setBounds(140, 175, 825, 25);
        formatoTextoSinCursorDerecha(txtDccionProveedor, panelProveedores);

        txtCPProveedor = new JTextField();
        txtCPProveedor.setBounds(75, 225, 100, 25);
        formatoTexto(txtCPProveedor, panelProveedores);

        txtPoblacionProveedor = new JTextField();
        txtPoblacionProveedor.setBounds(320, 225, 255, 25);
        formatoTextoSinCursorDerecha(txtPoblacionProveedor, panelProveedores);

        txtProvinciaProveedor = new JTextField();
        txtProvinciaProveedor.setBounds(710, 225, 255, 25);
        formatoTextoSinCursorDerecha(txtProvinciaProveedor, panelProveedores);

        txtBuscarProveedor = new JTextField();
        txtBuscarProveedor.setBounds(75, 275, 330, 25);
        formatoTextoSinCursorDerecha(txtBuscarProveedor, panelProveedores);

        scrollProveedores = new JScrollPane();
        scrollProveedores.setBounds(135, 340, 830, 375);
        panelProveedores.add(scrollProveedores);

        tablaProveedores = new JTable();
        scrollProveedores.setViewportView(tablaProveedores);
        tablaProveedores.setBackground(Color.WHITE);
    }

    /**
     * Método que pinta las etiquetas del panel de semirremolques
     */

    private void etiquetasSemirremolques() {
        lblMatriculaSemi = new JLabel("Matrícula");
        lblMatriculaSemi.setBounds(25, 125, 100, 25);
        formatoEtiquetas(lblMatriculaSemi, panelSemirremolques);

        lblITVSemi = new JLabel("ITV");
        lblITVSemi.setBounds(375, 125, 50, 25);
        formatoEtiquetas(lblITVSemi, panelSemirremolques);

        lblTipoSemi = new JLabel("Tipo");
        lblTipoSemi.setBounds(700, 125, 50, 25);
        formatoEtiquetas(lblTipoSemi, panelSemirremolques);

        lblSeguroSemi = new JLabel("Seguro");
        lblSeguroSemi.setBounds(25, 175, 100, 25);
        formatoEtiquetas(lblSeguroSemi, panelSemirremolques);

        lblBuscarSemi = new JLabel();
        lblBuscarSemi.setBounds(375, 164, 50, 50);
        lblBuscarSemi.setIcon(new ImageIcon("img/lupa.png"));
        panelSemirremolques.add(lblBuscarSemi);
    }

    /**
     * Método que pinta los campos del panel de semirremolques
     */

    private void camposSemirremolques() {
        txtMatriculaSemi = new JTextField();
        txtMatriculaSemi.setBounds(135, 125, 200, 25);
        formatoTexto(txtMatriculaSemi, panelSemirremolques);

        fechaITVSemi = new DatePicker();
        fechaITVSemi.setBounds(425, 125, 200, 25);
        panelSemirremolques.add(fechaITVSemi);

        comboTipoSemi = new JComboBox();
        comboTipoSemi.setBounds(765, 125, 200, 25);
        formatoCombo(comboTipoSemi, panelSemirremolques);

        fechaSeguroSemi = new DatePicker();
        fechaSeguroSemi.setBounds(135, 175, 200, 25);
        panelSemirremolques.add(fechaSeguroSemi);

        txtBuscarSemi = new JTextField();
        txtBuscarSemi.setBounds(425, 175, 255, 25);
        formatoTextoSinCursorDerecha(txtBuscarSemi, panelSemirremolques);

        scrollSemirremolques = new JScrollPane();
        scrollSemirremolques.setBounds(135, 340, 830, 375);
        panelSemirremolques.add(scrollSemirremolques);

        tablaSemirremolques = new JTable();
        scrollSemirremolques.setViewportView(tablaSemirremolques);
        tablaSemirremolques.setBackground(Color.WHITE);
    }

    /**
     * Método que pinta las etiquetas del panel de tractoras
     */

    private void etiquetasTractoras() {
        lblMatriculaTractora = new JLabel("Matrícula");
        lblMatriculaTractora.setBounds(25, 125, 100, 25);
        formatoEtiquetas(lblMatriculaTractora, panelTractoras);

        lblITVTractora = new JLabel("ITV");
        lblITVTractora.setBounds(350, 125, 50, 25);
        formatoEtiquetas(lblITVTractora, panelTractoras);

        lblLimitador = new JLabel("Limitador");
        lblLimitador.setBounds(650, 125, 100, 25);
        formatoEtiquetas(lblLimitador, panelTractoras);

        lblTacografo = new JLabel("Tacógrafo");
        lblTacografo.setBounds(25, 175, 100, 25);
        formatoEtiquetas(lblTacografo, panelTractoras);

        lblSeguroTractora = new JLabel("Seguro");
        lblSeguroTractora.setBounds(350, 175, 100, 25);
        formatoEtiquetas(lblSeguroTractora, panelTractoras);

        lblTarjetaTte = new JLabel("Tarjeta Tte.");
        lblTarjetaTte.setBounds(650, 175, 175, 25);
        formatoEtiquetas(lblTarjetaTte, panelTractoras);

        lblBuscarTractora = new JLabel();
        lblBuscarTractora.setBounds(25, 214, 50, 50);
        lblBuscarTractora.setIcon(new ImageIcon("img/lupa.png"));
        panelTractoras.add(lblBuscarTractora);
    }

    /**
     * Método que pinta los campos del panel de tractoras
     */

    private void camposTractoras() {
        txtMatriculaTractora = new JTextField();
        txtMatriculaTractora.setBounds(135, 125, 200, 25);
        formatoTexto(txtMatriculaTractora, panelTractoras);

        fechaITVTractora = new DatePicker();
        fechaITVTractora.setBounds(430, 125, 200, 25);
        panelTractoras.add(fechaITVTractora);

        fechaLimitador = new DatePicker();
        fechaLimitador.setBounds(765, 125, 200, 25);
        panelTractoras.add(fechaLimitador);

        fechaTacografo = new DatePicker();
        fechaTacografo.setBounds(135, 175, 200, 25);
        panelTractoras.add(fechaTacografo);

        fechaSeguroTractora = new DatePicker();
        fechaSeguroTractora.setBounds(430, 175, 200, 25);
        panelTractoras.add(fechaSeguroTractora);

        txtTarjetaTte = new JTextField();
        txtTarjetaTte.setBounds(765, 175, 200, 25);
        formatoTexto(txtTarjetaTte, panelTractoras);

        txtBuscarTractora = new JTextField();
        txtBuscarTractora.setBounds(75, 225, 255, 25);
        formatoTextoSinCursorDerecha(txtBuscarTractora, panelTractoras);

        scrollTractoras = new JScrollPane();
        scrollTractoras.setBounds(135, 340, 830, 375);
        panelTractoras.add(scrollTractoras);

        tablaTractoras = new JTable();
        scrollTractoras.setViewportView(tablaTractoras);
        scrollTractoras.setBackground(Color.WHITE);
    }

    /**
     * Método que pinta las etiquetas en el panel de viajes
     */

    private void etiquetasViajes() {
        lblMatriculaViaje = new JLabel("Matrícula");
        lblMatriculaViaje.setBounds(25, 125, 100, 25);
        formatoEtiquetas(lblMatriculaViaje, panelViajes);

        lblSemirremolque = new JLabel("Semirremolque");
        lblSemirremolque.setBounds(350, 125, 150, 25);
        formatoEtiquetas(lblSemirremolque, panelViajes);

        lblAlbaran = new JLabel("Albarán");
        lblAlbaran.setBounds(690, 125, 75, 25);
        formatoEtiquetas(lblAlbaran, panelViajes);

        lblFechaViaje = new JLabel("Fecha");
        lblFechaViaje.setBounds(25, 175, 75, 25);
        formatoEtiquetas(lblFechaViaje, panelViajes);

        lblChoferViaje = new JLabel("Chófer");
        lblChoferViaje.setBounds(350, 175, 75, 25);
        formatoEtiquetas(lblChoferViaje, panelViajes);

        lblPrecio = new JLabel("Precio");
        lblPrecio.setBounds(700, 175, 75, 25);
        formatoEtiquetas(lblPrecio, panelViajes);

        lblOrigen = new JLabel("Orígen");
        lblOrigen.setBounds(25, 225, 75, 25);
        formatoEtiquetas(lblOrigen, panelViajes);

        lblDestino = new JLabel("Destino");
        lblDestino.setBounds(520, 225, 100, 25);
        formatoEtiquetas(lblDestino, panelViajes);

        lblClienteViaje = new JLabel("Cliente");
        lblClienteViaje.setBounds(25, 275, 100, 25);
        formatoEtiquetas(lblClienteViaje, panelViajes);

        lblPeso = new JLabel("Kg");
        lblPeso.setBounds(450, 275, 75, 25);
        formatoEtiquetas(lblPeso, panelViajes);

        lblBuscarViaje = new JLabel();
        lblBuscarViaje.setBounds(610, 264, 50, 50);
        lblBuscarViaje.setIcon(new ImageIcon("img/lupa.png"));
        formatoEtiquetas(lblBuscarViaje, panelViajes);
    }

    /**
     * Método que pinta los campos del panel de viajes
     */

    private void camposViajes() {
        comboMatricula = new JComboBox();
        comboMatricula.setBounds(135, 125, 150, 25);
        formatoCombo(comboMatricula, panelViajes);

        comboSemi = new JComboBox();
        comboSemi.setBounds(510, 125, 150, 25);
        formatoCombo(comboSemi, panelViajes);

        txtAlbaran = new JTextField();
        txtAlbaran.setBounds(790, 125, 175, 25);
        formatoTexto(txtAlbaran, panelViajes);

        fechaViaje = new DatePicker();
        fechaViaje.setBounds(135, 175, 190, 25);
        panelViajes.add(fechaViaje);

        comboChofer = new JComboBox();
        comboChofer.setBounds(460, 175, 200, 25);
        formatoCombo(comboChofer, panelViajes);

        txtPrecio = new JTextField();
        txtPrecio.setBounds(790, 175, 175, 25);
        formatoTexto(txtPrecio, panelViajes);

        txtOrigen = new JTextField();
        txtOrigen.setBounds(135, 225, 330, 25);
        formatoTextoSinCursorDerecha(txtOrigen, panelViajes);

        txtDestino = new JTextField();
        txtDestino.setBounds(635, 225, 330, 25);
        formatoTextoSinCursorDerecha(txtDestino, panelViajes);

        comboCliente = new JComboBox();
        comboCliente.setBounds(135, 275, 275, 25);
        formatoCombo(comboCliente, panelViajes);

        txtPeso = new JTextField();
        txtPeso.setBounds(500, 275, 75, 25);
        formatoTexto(txtPeso, panelViajes);

        txtBuscarViaje = new JTextField();
        txtBuscarViaje.setBounds(665, 275, 300, 25);
        formatoTextoSinCursorDerecha(txtBuscarViaje, panelViajes);

        scrollViajes = new JScrollPane();
        scrollViajes.setBounds(135, 340, 830, 375);
        panelViajes.add(scrollViajes);

        tablaViajes = new JTable();
        scrollViajes.setViewportView(tablaViajes);
        scrollViajes.setBackground(Color.WHITE);
    }

    /**
     * Método que pinta las etiquetas del panel de viajes vendidos
     */

    private void etiquetasVendidos() {

        lblProveedorViaje = new JLabel("Subcontratado");
        lblProveedorViaje.setBounds(25, 125, 150, 25);
        formatoEtiquetas(lblProveedorViaje, panelVendidos);

        lblClienteProveedor = new JLabel("Cliente");
        lblClienteProveedor.setBounds(545, 125, 100, 25);
        formatoEtiquetas(lblClienteProveedor, panelVendidos);

        lblFechaViajeProveedor = new JLabel("Fecha");
        lblFechaViajeProveedor.setBounds(25, 175, 75, 25);
        formatoEtiquetas(lblFechaViajeProveedor, panelVendidos);

        lblMatriculaProveedor = new JLabel("Matrícula");
        lblMatriculaProveedor.setBounds(360, 175, 100, 25);
        formatoEtiquetas(lblMatriculaProveedor, panelVendidos);

        lblSemiProveedor = new JLabel("Semirremolque");
        lblSemiProveedor.setBounds(650, 175, 150, 25);
        formatoEtiquetas(lblSemiProveedor, panelVendidos);

        lblOrigenProveedor = new JLabel("Orígen");
        lblOrigenProveedor.setBounds(25, 225, 75, 25);
        formatoEtiquetas(lblOrigenProveedor, panelVendidos);

        lblDestinoProveedor = new JLabel("Destino");
        lblDestinoProveedor.setBounds(510, 225, 75, 25);
        formatoEtiquetas(lblDestinoProveedor, panelVendidos);

        lblAlbaranProveedor = new JLabel("Albarán");
        lblAlbaranProveedor.setBounds(25, 275, 75, 25);
        formatoEtiquetas(lblAlbaranProveedor, panelVendidos);

        lblPesoProveedor = new JLabel("Kg");
        lblPesoProveedor.setBounds(310, 275, 75, 25);
        formatoEtiquetas(lblPesoProveedor, panelVendidos);

        lblPrecioProveedor = new JLabel("Precio");
        lblPrecioProveedor.setBounds(470, 275, 75, 25);
        formatoEtiquetas(lblPrecioProveedor, panelVendidos);

        lblBuscarViajeProveedor = new JLabel();
        lblBuscarViajeProveedor.setBounds(650, 264, 50, 50);
        lblBuscarViajeProveedor.setIcon(new ImageIcon("img/lupa.png"));
        panelVendidos.add(lblBuscarViajeProveedor);
    }

    /**
     * Método que pinta los campos del panel de viajes vendidos
     */

    private void camposViajesVendidos() {
        comboProveedor = new JComboBox();
        comboProveedor.setBounds(180, 125, 340, 25);
        formatoCombo(comboProveedor, panelVendidos);

        comboClienteProveedor = new JComboBox();
        comboClienteProveedor.setBounds(625, 125, 340, 25);
        formatoCombo(comboClienteProveedor, panelVendidos);

        fechaViajeProveedor = new DatePicker();
        fechaViajeProveedor.setBounds(135, 175, 190, 25);
        panelVendidos.add(fechaViajeProveedor);

        txtMatriculaViajeProveedor = new JTextField();
        txtMatriculaViajeProveedor.setBounds(470, 175, 150, 25);
        formatoTexto(txtMatriculaViajeProveedor, panelVendidos);

        txtMatriculaSemiViajeProveedor = new JTextField();
        txtMatriculaSemiViajeProveedor.setBounds(815, 175, 150, 25);
        formatoTexto(txtMatriculaSemiViajeProveedor, panelVendidos);

        txtOrigenProveedor = new JTextField();
        txtOrigenProveedor.setBounds(115, 225, 360, 25);
        formatoTextoSinCursorDerecha(txtOrigenProveedor, panelVendidos);

        txtDestinoProveedor = new JTextField();
        txtDestinoProveedor.setBounds(605, 225, 360, 25);
        formatoTextoSinCursorDerecha(txtDestinoProveedor, panelVendidos);

        txtAlbaranProveedor = new JTextField();
        txtAlbaranProveedor.setBounds(125, 275, 150, 25);
        formatoTexto(txtAlbaranProveedor, panelVendidos);

        txtPesoProveedor = new JTextField();
        txtPesoProveedor.setBounds(360, 275, 75, 25);
        formatoTexto(txtPesoProveedor, panelVendidos);

        txtPrecioProveedor = new JTextField();
        txtPrecioProveedor.setBounds(550, 275, 75, 25);
        formatoTexto(txtPrecioProveedor, panelVendidos);

        txtPrecioProveedor = new JTextField();
        txtPrecioProveedor.setBounds(550, 275, 75, 25);
        formatoTexto(txtPrecioProveedor, panelVendidos);

        txtBuscarViajeProveedor = new JTextField();
        txtBuscarViajeProveedor.setBounds(700, 275, 265, 25);
        formatoTextoSinCursorDerecha(txtBuscarViajeProveedor, panelVendidos);

        scrollVendidos = new JScrollPane();
        scrollVendidos.setBounds(135, 340, 830, 375);
        panelVendidos.add(scrollVendidos);

        tablaVendidos = new JTable();
        scrollVendidos.setViewportView(tablaVendidos);
        scrollVendidos.setBackground(Color.WHITE);
    }

    /**
     * Método para automatizar los elementos comunes del formato de muchas etiquetas como la fuente y tamaño
     * @param label es la etiqueta
     * @param panel el panel al que pertenece la etiqueta
     */

    private void formatoEtiquetas(JLabel label, JPanel panel) {
        label.setFont(new Font("Sans Serif", Font.BOLD, 20));
        panel.add(label);
    }

    /**
     * Método para automatizar los elementos comunes de los campos como fuente, tamaño y posición de cursor a la derecha
     * @param jTextField es el campo
     * @param panel es el panel al que pertenece el campo
     */

    private void formatoTexto(JTextField jTextField, JPanel panel) {
        jTextField.setBackground(Color.WHITE);
        jTextField.setFont(new Font("Sans Serif", Font.PLAIN, 20));
        jTextField.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        panel.add(jTextField);
    }

    /**
     * Método para automatizar los elementos comunes de los campos como fuente, tamaño y posición de cursor a la izquierda
     * @param jTextField es el campo
     * @param panel es el panel al que pertenece el campo
     */

    private void formatoTextoSinCursorDerecha(JTextField jTextField, JPanel panel) {
        jTextField.setBackground(Color.WHITE);
        jTextField.setFont(new Font("Sans Serif", Font.PLAIN, 20));
        panel.add(jTextField);
    }

    /**
     * Método para automatizar los elementos comunes de los desplegables como fuente, tamaño y color de fondo
     * @param combo es el combobox o desplegable
     * @param panel es el panel al que pertenece
     */

    private void formatoCombo(JComboBox combo, JPanel panel) {
        combo.setBackground(Color.WHITE);
        combo.setFont(new Font("Sans Serif", Font.PLAIN, 20));
        panel.add(combo);
    }

    /**
     * Método para automatizar los elementos comunes de los botones como imagen, tamaño y posición
     * @param addButton es el botón de guardar
     * @param modButton es el botón de modificar
     * @param delButton es el botón de borrar
     * @param listButton es el botón de listar
     * @param panel es el panel al que pertenece el botón
     */

    private void formatoBotones(JButton addButton, JButton modButton, JButton delButton, JButton listButton, JPanel panel) {

        addButton= new JButton();
        addButton.setBounds(20, 355, 100, 66);
        addButton.setIcon(new ImageIcon("img/botonguardar.png"));
        transpareciaBotones(addButton, panel);


        modButton = new JButton();
        modButton.setBounds(20, 450, 100, 66);
        modButton.setIcon(new ImageIcon("img/botonmodificar.png"));
        transpareciaBotones(modButton, panel);

        delButton = new JButton();
        delButton.setBounds(20, 545, 100, 66);
        delButton.setIcon(new ImageIcon("img/botonborrar.png"));
        transpareciaBotones(delButton, panel);

        listButton = new JButton();
        listButton.setBounds(20, 640, 100, 66);
        listButton.setIcon(new ImageIcon("img/botonlistar.png"));
        transpareciaBotones(listButton, panel);
    }

    /**
     * Método para automatizar aquellos elementos comunes de los botones que concierne a transparencia tanto de fondo
     * como de borde
     * @param button es el botón
     * @param panel es el panel al que pertenece el botón
     */

    private void transpareciaBotones (JButton button, JPanel panel) {
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        panel.add(button);
    }

    /**
     * Método encargado de colocar el menú superior con las distintas opciones, como cambio de usuario o salir
     */

    private void crearMenu() {
        JMenuBar menuBar = new JMenuBar();
        menuArchivo = new JMenu("Archivo");
        menuArchivo.setFont(new Font("Sans Serif", Font.BOLD, 15));
        itemOpciones = new JMenuItem("Opciones");
        addItems(itemOpciones, "Opciones");
        itemSalir = new JMenuItem("Salir");
        addItems(itemSalir, "Salir");
        itemUsuarios = new JMenuItem("Cambio de usuario");
        addItems(itemUsuarios, "Cambio de usuario");

        menuBar.add(menuArchivo);
        menuBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(menuBar);

    }

    /**
     * Método que coloca los elementos del menú
     * @param item es el elemento o item del menú
     * @param nombreItem es el nombre del ítem
     */

    private void addItems(JMenuItem item, String nombreItem) {
        item.setActionCommand(nombreItem);
        item.setFont(new Font("Sans Serif", Font.BOLD, 15));
        menuArchivo.add(item);
    }

    /**
     * Mëtodo que construye el desplegable de tipos de semirremolque en función de una enumeración creada dentro del
     * paquete gui
     */

    private void setEnumCombo() {
        for (TipoPlataformas constant : TipoPlataformas.values()) {
            comboTipoSemi.addItem(constant.getValor());
        }

        comboTipoSemi.setSelectedIndex(-1);

    }

    /**
     * Método que pinta la tabla de chóferes, incluyendo las cabeceras de cada columna. Se añade la propiedad de
     * ordenación por columna cliqueada
     */

    private void iniciarTablaChoferes() {
        dtmChoferes = new DefaultTableModel();
        tablaChoferes.setModel(dtmChoferes);
        Object[] cabeceras = {"Nombre", "Apellidos", "Fecha nac", "Seg.Soc.", "DNI", "Fecha inicio", "Fecha fin",
                "Tfno", "Dccion"};
        dtmChoferes.setColumnIdentifiers(cabeceras);
        TableRowSorter<TableModel> sorter=new TableRowSorter<TableModel>(dtmChoferes);
        tablaChoferes.setRowSorter(sorter);
    }

    /**
     * Método que pinta la tabla de clientes, incluyendo las cabeceras de cada columna. Se añade la propiedad de
     * ordenación por columna cliqueada
     */

    private void iniciarTablaClientes() {
        dtmClientes = new DefaultTableModel();
        tablaClientes.setModel(dtmClientes);
        Object[] cabeceras = {"Nombre", "CIF", "Tfno", "Dcción", "CP", "Población", "Provincia"};
        dtmClientes.setColumnIdentifiers(cabeceras);
        TableRowSorter<TableModel> sorter=new TableRowSorter<TableModel>(dtmClientes);
        tablaClientes.setRowSorter(sorter);
    }

    /**
     * Método que pinta la tabla de proveedores, incluyendo las cabeceras de cada columna. Se añade la propiedad de
     * ordenación por columna cliqueada
     */

    private void iniciarTablaProveedores() {
        dtmProveedores = new DefaultTableModel();
        tablaProveedores.setModel(dtmProveedores);
        Object[] cabeceras = {"Nombre", "CIF", "Tfno", "Dcción", "CP", "Población", "Provincia"};
        dtmProveedores.setColumnIdentifiers(cabeceras);
        TableRowSorter<TableModel> sorter=new TableRowSorter<TableModel>(dtmProveedores);
        tablaProveedores.setRowSorter(sorter);
    }

    /**
     * Método que pinta la tabla de semirremolques, incluyendo las cabeceras de cada columna. Se añade la propiedad de
     * ordenación por columna cliqueada
     */

    private void iniciarTablaSemirremolques() {
        dtmSemirremolques = new DefaultTableModel();
        tablaSemirremolques.setModel(dtmSemirremolques);
        Object[] cabeceras = {"Matrícula", "ITV", "Tipo", "Seguro"};
        dtmSemirremolques.setColumnIdentifiers(cabeceras);
        TableRowSorter<TableModel> sorter=new TableRowSorter<TableModel>(dtmSemirremolques);
        tablaSemirremolques.setRowSorter(sorter);
    }

    /**
     * Método que pinta la tabla de tractoras, incluyendo las cabeceras de cada columna. Se añade la propiedad de
     * ordenación por columna cliqueada
     */

    private void iniciarTablaTractoras() {
        dtmTractoras = new DefaultTableModel();
        tablaTractoras.setModel(dtmTractoras);
        tablaTractoras.setFont(new Font("Sans Serif", Font.PLAIN, 20));
        Object[] cabeceras = {"Matrícula", "ITV", "Limitador", "Tacógrafo", "Seguro", "Tarjeta Tte"};
        dtmTractoras.setColumnIdentifiers(cabeceras);
        TableRowSorter<TableModel> sorter=new TableRowSorter<TableModel>(dtmTractoras);
        tablaTractoras.setRowSorter(sorter);
    }

    /**
     * Método que pinta la tabla de viajes, incluyendo las cabeceras de cada columna. Se añade la propiedad de
     * ordenación por columna cliqueada
     */

    private void iniciarTablaViajes() {
        dtmViajes = new DefaultTableModel();
        tablaViajes.setModel(dtmViajes);
        tablaViajes.setFont(new Font("Sans Serif", Font.PLAIN, 20));
        Object[] cabeceras = {"Matrícula", "Semirremolque", "Albarán", "Fecha", "Chófer", "Precio", "Orígen", "Destino",
                "Cliente", "Kg"};
        dtmViajes.setColumnIdentifiers(cabeceras);
        TableRowSorter<TableModel> sorter=new TableRowSorter<TableModel>(dtmViajes);
        tablaViajes.setRowSorter(sorter);
    }

    /**
     * Método que pinta la tabla de los viajes desempeñados por los proveedores, incluyendo las cabeceras de cada
     * columna. Se añade la propiedad de ordenación por columna cliqueada
     */

    private void iniciarTablaVendidos(){
        dtmVendidos = new DefaultTableModel();
        tablaVendidos.setModel(dtmVendidos);
        tablaVendidos.setFont(new Font("Sans Serif", Font.PLAIN, 20));
        Object[] cabeceras = {"Subcontratado", "Cliente", "Matrícula", "Fecha", "Semirremolque", "Orígen", "Destino",
                "Albarán", "Kg", "Precio"};
        dtmVendidos.setColumnIdentifiers(cabeceras);
        TableRowSorter<TableModel> sorter=new TableRowSorter<TableModel>(dtmVendidos);
        tablaVendidos.setRowSorter(sorter);
    }
}