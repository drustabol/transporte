package gui;

import hibernate.Chofer;
import hibernate.Cliente;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Date;

public class ControladorUsuario implements ActionListener, TableModelListener, MouseListener {

    private VistaUsuario vistaUsuario;
    private Modelo modelo;

    public ControladorUsuario(Modelo modelo, VistaUsuario vistaUsuario) {

        this.vistaUsuario = vistaUsuario;
        this.modelo = modelo;

        addActionListeners(this);
        addTableModelListener(this);
        aadMouseListener(this);
    }

    private void aadMouseListener(ControladorUsuario controladorUsuario) {
        vistaUsuario.tablaClientes.addMouseListener(controladorUsuario);
        vistaUsuario.tablaProveedores.addMouseListener(controladorUsuario);
        vistaUsuario.tablaSemirremolques.addMouseListener(controladorUsuario);
        vistaUsuario.tablaTractoras.addMouseListener(controladorUsuario);
        vistaUsuario.tablaViajes.addMouseListener(controladorUsuario);
        vistaUsuario.tablaVendidos.addMouseListener(controladorUsuario);
    }

    private void addTableModelListener(TableModelListener listener) {
        vistaUsuario.dtmClientes.addTableModelListener(listener);
        vistaUsuario.dtmProveedores.addTableModelListener(listener);
        vistaUsuario.dtmSemirremolques.addTableModelListener(listener);
        vistaUsuario.dtmTractoras.addTableModelListener(listener);
        vistaUsuario.dtmViajes.addTableModelListener(listener);
        vistaUsuario.dtmVendidos.addTableModelListener(listener);
    }

    private void addActionListeners(ActionListener listener) {
        //Listener de Chóferes
        vistaUsuario.btnListChofer.addActionListener(listener);

        //Listener de Clientes
        vistaUsuario.btnAddCliente.addActionListener(listener);
        vistaUsuario.btnModCliente.addActionListener(listener);
        vistaUsuario.btnDelCliente.addActionListener(listener);
        vistaUsuario.btnListCliente.addActionListener(listener);


        //Listener de Proveedores
        vistaUsuario.btnAddProveedor.addActionListener(listener);
        vistaUsuario.btnModProveedor.addActionListener(listener);
        vistaUsuario.btnDelProveedor.addActionListener(listener);
        vistaUsuario.btnListProveedor.addActionListener(listener);

        //Listener de Semirremolques
        vistaUsuario.btnAddSemi.addActionListener(listener);
        vistaUsuario.btnModSemi.addActionListener(listener);
        vistaUsuario.btnDelSemi.addActionListener(listener);
        vistaUsuario.btnListSemi.addActionListener(listener);

        //Listener de Tractoras
        vistaUsuario.btnAddTractora.addActionListener(listener);
        vistaUsuario.btnModTractora.addActionListener(listener);
        vistaUsuario.btnDelTractora.addActionListener(listener);
        vistaUsuario.btnListTractora.addActionListener(listener);

        //Listener de Viajes
        vistaUsuario.btnAddViaje.addActionListener(listener);
        vistaUsuario.btnModViaje.addActionListener(listener);
        vistaUsuario.btnDelViaje.addActionListener(listener);
        vistaUsuario.btnListViaje.addActionListener(listener);

        //Listener menú
        vistaUsuario.conexionItem.addActionListener(listener);
        vistaUsuario.itemSalir.addActionListener(listener);


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        modelo = new Modelo();
        String comando = e.getActionCommand();

        switch (comando) {

            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;

            case "Conectar":
                vistaUsuario.conexionItem.setEnabled(false);
                modelo.conectar();
                break;

            case "addInstrumentobtn":
                Instrumento nuevoInstrumento = new Instrumento();
                nuevoInstrumento.setCodigo(vistaUsuario.txtCodigo.getText());
                nuevoInstrumento.setTipo(String.valueOf(vistaUsuario.comboTipo.getSelectedItem()));
                nuevoInstrumento.setNombre(vistaUsuario.txtNombre.getText());
                try {
                    nuevoInstrumento.setPvp(Double.parseDouble(vistaUsuario.txtPvp.getText()));
                } catch (NumberFormatException ex) {
                    util.showErrorAlert("Introduce números en precio");
                }
                nuevoInstrumento.setFechaAdquisicion(Date.valueOf(vistaUsuario.datePicker.getDate()));
                nuevoInstrumento.setPropietario((Propietario) vistaUsuario.comboPropietarios.getSelectedItem());

                modelo.insertar(nuevoInstrumento);
                limpiarCamposInstrumentos();
                listarInstrumentos(modelo.getInstrumentos());

                break;

            case "btnModCliente":
                Cliente clienteSeleccionado = (Cliente) vistaUsuario.tablaClientes.getSelectedItem();
                instrumentoSeleccionado.setCodigo(vistaUsuario.txtCodigo.getText());
                instrumentoSeleccionado.setTipo(String.valueOf(vistaUsuario.comboTipo.getSelectedItem()));
                instrumentoSeleccionado.setNombre(vistaUsuario.txtNombre.getText());
                try {
                    instrumentoSeleccionado.setPvp(Double.parseDouble(vistaUsuario.txtPvp.getText()));
                } catch (NumberFormatException e1) {
                    util.showErrorAlert("Introduce números en precio");
                }
                instrumentoSeleccionado.setFechaAdquisicion(Date.valueOf(vistaUsuario.datePicker.getDate()));
                instrumentoSeleccionado.setPropietario((Propietario) vistaUsuario.comboPropietarios.getSelectedItem());
                modelo.modificar(instrumentoSeleccionado);
                limpiarCamposInstrumentos();
                listarInstrumentos(modelo.getInstrumentos());

                break;

            case "eliminarInstrumentoBtn":
                Instrumento instrumentoBorrado = (Instrumento) vistaUsuario.listaInstrumentos.getSelectedValue();
                modelo.eliminar(instrumentoBorrado);
                limpiarCamposInstrumentos();
                listarInstrumentos(modelo.getInstrumentos());

                break;

            case "btnlistadoInstrumentos":
                listarInstrumentos(modelo.getInstrumentos());
                break;

            case "addMarcaBtn":
                Marca nuevaMarca = new Marca();
                nuevaMarca.setNombremarca(vistaUsuario.txtNombreMarca.getText());
                nuevaMarca.setPais(String.valueOf(vistaUsuario.comboPais.getSelectedItem()));
                nuevaMarca.setDelegacion(vistaUsuario.txtDelegacion.getText());
                nuevaMarca.setWeb(vistaUsuario.txtWeb.getText());
                modelo.insertar(nuevaMarca);
                limpiarCamposMarcas();
                listarMarcas(modelo.getMarcas());

                break;

            case "modificarMarcaBtn":
                Marca marcaSeleccionada = (Marca) vistaUsuario.listaMarcas.getSelectedValue();
                marcaSeleccionada.setNombremarca(vistaUsuario.txtNombreMarca.getText());
                marcaSeleccionada.setPais(String.valueOf(vistaUsuario.comboPais.getSelectedItem()));
                marcaSeleccionada.setDelegacion(vistaUsuario.txtDelegacion.getText());
                marcaSeleccionada.setWeb(vistaUsuario.txtWeb.getText());
                modelo.modificar(marcaSeleccionada);
                limpiarCamposMarcas();
                listarMarcas(modelo.getMarcas());
                break;

            case "eliminarMarcaBtn":
                Marca marcaBorrada = (Marca) vistaUsuario.listaMarcas.getSelectedValue();
                modelo.eliminar(marcaBorrada);
                limpiarCamposMarcas();
                listarMarcas(modelo.getMarcas());
                break;

            case "btnListadoMarcas":
                listarMarcas(modelo.getMarcas());
                break;

            case "addMaterialBtn":
                Material nuevoMaterial = new Material();
                nuevoMaterial.setNombreMaterial(vistaUsuario.txtNombreMaterial.getText());
                nuevoMaterial.setCalidad(String.valueOf(vistaUsuario.comboCalidad.getSelectedItem()));
                if (vistaUsuario.rbtnNo.isSelected()) {
                    nuevoMaterial.setOrganico("No");
                }
                if (vistaUsuario.rbtnSi.isSelected()) {
                    nuevoMaterial.setOrganico("Si");
                }
                nuevoMaterial.setProcedencia(vistaUsuario.txtProcedencia.getText());
                modelo.insertar(nuevoMaterial);
                limpiarCamposMateriales();
                listarMateriales(modelo.getMateriales());
                break;

            case "modificarMaterialBtn":
                Material materialSeleccionado = (Material) vistaUsuario.listaMateriales.getSelectedValue();
                materialSeleccionado.setNombreMaterial(vistaUsuario.txtNombreMaterial.getText());
                materialSeleccionado.setCalidad(String.valueOf(vistaUsuario.comboCalidad.getSelectedItem()));
                if (vistaUsuario.rbtnNo.isSelected()) {
                    materialSeleccionado.setOrganico("No");
                }
                if (vistaUsuario.rbtnSi.isSelected()) {
                    materialSeleccionado.setOrganico("Si");
                }
                materialSeleccionado.setProcedencia(vistaUsuario.txtProcedencia.getText());
                modelo.modificar(materialSeleccionado);
                limpiarCamposMateriales();
                listarMateriales(modelo.getMateriales());
                break;

            case "eliminarMaterialBtn":
                Material materialBorrado = (Material) vistaUsuario.listaMateriales.getSelectedValue();
                modelo.eliminar(materialBorrado);
                limpiarCamposMarcas();
                listarMateriales(modelo.getMateriales());
                break;

            case "btnlistadoMateriales":
                listarMateriales(modelo.getMateriales());
                break;

            case "addPropietarioBtn":
                Propietario nuevoPropietario = new Propietario();
                nuevoPropietario.setNombre(vistaUsuario.txtNombrePropietario.getText());
                nuevoPropietario.setApellidos(vistaUsuario.txtApellidos.getText());
                nuevoPropietario.setDccion(vistaUsuario.txtDccion.getText());
                nuevoPropietario.setTfno(Integer.parseInt(vistaUsuario.txtTfno.getText()));
                modelo.insertar(nuevoPropietario);
                limpiarCamposPropietarios();
                listarPropietarios(modelo.getPropietarios());
                break;

            case "modificarPropietarioBtn":
                Propietario propietarioSeleccionado = (Propietario) vistaUsuario.listaPropietarios.getSelectedValue();
                propietarioSeleccionado.setNombre(vistaUsuario.txtNombrePropietario.getText());
                propietarioSeleccionado.setApellidos(vistaUsuario.txtApellidos.getText());
                propietarioSeleccionado.setDccion(vistaUsuario.txtDccion.getText());
                propietarioSeleccionado.setTfno(Integer.parseInt(vistaUsuario.txtTfno.getText()));
                modelo.modificar(propietarioSeleccionado);
                limpiarCamposPropietarios();
                listarPropietarios(modelo.getPropietarios());

                break;

            case "eliminarPropietarioBtn":
                Propietario borrarPropietario = (Propietario) vistaUsuario.listaPropietarios.getSelectedValue();
                modelo.eliminar(borrarPropietario);
                limpiarCamposPropietarios();
                listarPropietarios(modelo.getPropietarios());
                break;

            case "btnlistadoPropietarios":
                listarPropietarios(modelo.getPropietarios());
                break;

            case "insertarFabBtn":
                Instrumentomateriales nuevoFabricado = new Instrumentomateriales();
                try {
                    nuevoFabricado.setCantidad(Integer.parseInt(vistaUsuario.txtCantidad.getText()));
                } catch (NumberFormatException ex) {
                    util.showErrorAlert("Introduce números en precio");
                }
                nuevoFabricado.setInstrumento((Instrumento) vistaUsuario.comboInstrumentos.getSelectedItem());
                nuevoFabricado.setMaterial((Material) vistaUsuario.comboMaterial.getSelectedItem());
                modelo.insertar(nuevoFabricado);
                limpiarCamposFabricados();
                listarFabricados(modelo.getFabricaciones());

                break;

            case "modificarFabBtn":
                Instrumentomateriales fabricadoSeleccionado = (Instrumentomateriales) vistaUsuario.listaFabricados.getSelectedValue();
                try {
                    fabricadoSeleccionado.setCantidad(Integer.parseInt(vistaUsuario.txtCantidad.getText()));
                } catch (NumberFormatException ex) {
                    util.showErrorAlert("Introduce números en precio");
                }
                fabricadoSeleccionado.setInstrumento((Instrumento) vistaUsuario.comboInstrumentos.getSelectedItem());
                fabricadoSeleccionado.setMaterial((Material) vistaUsuario.comboMaterial.getSelectedItem());
                modelo.modificar(fabricadoSeleccionado);
                limpiarCamposFabricados();
                listarFabricados(modelo.getFabricaciones());
                break;

            case "borrarFabBtn":
                Instrumentomateriales borrarFabricado = (Instrumentomateriales) vistaUsuario.listaFabricados.getSelectedValue();
                modelo.eliminar(borrarFabricado);
                limpiarCamposFabricados();
                listarFabricados(modelo.getFabricaciones());
                break;

            case "btnListadoFabricados":
                listarFabricados(modelo.getFabricaciones());
                break;

            case "instrumentosBtn":
                Propietario prop= (Propietario) vistaUsuario.listaPropietarios.getSelectedValue();
                prop.setInstrumento((ArrayList)modelo.getPropietariosInstrumento(prop));
                listarPropietarioInstrumentos(modelo.getPropietariosInstrumento(prop));
                break;

        }
        limpiarTodo();
        listarTodo();
    }

    private void limpiarTodo(){
        limpiarCamposPropietarios();
        limpiarCamposMarcas();
        limpiarCamposInstrumentos();
        limpiarCamposFabricados();
        limpiarCamposMateriales();
    }

    private void limpiarCamposFabricados() {
        vistaUsuario.txtCantidad.setText("");
        vistaUsuario.comboInstrumentos.setSelectedIndex(-1);
        vistaUsuario.comboMaterial.setSelectedIndex(-1);

    }

    private void limpiarCamposPropietarios() {
        vistaUsuario.txtNombrePropietario.setText("");
        vistaUsuario.txtApellidos.setText("");
        vistaUsuario.txtDccion.setText("");
        vistaUsuario.txtTfno.setText("");
    }

    private void limpiarCamposMateriales() {
        vistaUsuario.txtNombreMaterial.setText("");
        vistaUsuario.comboCalidad.setSelectedIndex(-1);
        vistaUsuario.rbtnSi.setSelected(false);
        vistaUsuario.rbtnNo.setSelected(false);
        vistaUsuario.txtProcedencia.setText("");
    }

    private void limpiarCamposMarcas() {
        vistaUsuario.txtNombreMarca.setText("");
        vistaUsuario.comboPais.setSelectedIndex(-1);
        vistaUsuario.txtDelegacion.setText("");
        vistaUsuario.txtWeb.setText("");
    }

    private void limpiarCamposInstrumentos() {
        vistaUsuario.txtCodigo.setText("");
        vistaUsuario.comboTipo.setSelectedIndex(-1);
        vistaUsuario.txtNombre.setText("");
        vistaUsuario.txtPvp.setText("");
        vistaUsuario.datePicker.setText("");
        vistaUsuario.comboPropietarios.setSelectedIndex(-1);
    }

    private void listarTodo(){
        listarInstrumentos(modelo.getInstrumentos());
        listarMateriales(modelo.getMateriales());
        listarMarcas(modelo.getMarcas());
        listarFabricados(modelo.getFabricaciones());
        listarPropietarios(modelo.getPropietarios());

    }

    private void listarInstrumentos(ArrayList<Instrumento> instrumentos) {
        vistaUsuario.dlmInstrumentos.clear();
        for (Instrumento instrumento : instrumentos) {
            vistaUsuario.dlmInstrumentos.addElement(instrumento);
        }

        vistaUsuario.comboInstrumentos.removeAllItems();
        ArrayList<Instrumento> ins=modelo.getInstrumentos();
        for(Instrumento i: ins) {
            vistaUsuario.comboInstrumentos.addItem(i);
        }
        vistaUsuario.comboInstrumentos.setSelectedItem(-1);

    }

    public void listarMarcas(ArrayList<Marca> marcas) {
        vistaUsuario.dlmMarca.clear();
        for (Marca marca : marcas) {
            vistaUsuario.dlmMarca.addElement(marca);
        }
    }

    public void listarMateriales(ArrayList<Material> materiales) {
        vistaUsuario.dlmMaterial.clear();
        for (Material material : materiales) {
            vistaUsuario.dlmMaterial.addElement(material);
        }

        vistaUsuario.comboMaterial.removeAllItems();
        ArrayList<Material> mat=modelo.getMateriales();
        for(Material m: mat) {
            vistaUsuario.comboInstrumentos.addItem(m);
        }
        vistaUsuario.comboMaterial.setSelectedItem(-1);
    }

    public void listarPropietarios(ArrayList<Propietario> propietarios) {
        vistaUsuario.dlmPropietario.clear();
        for (Propietario propietario : propietarios) {
            vistaUsuario.dlmPropietario.addElement(propietario);
        }

        vistaUsuario.comboPropietarios.removeAllItems();
        ArrayList<Propietario> prop=modelo.getPropietarios();
        for (Propietario pr:prop){
            vistaUsuario.comboPropietarios.addItem(pr);
        }
        vistaUsuario.comboPropietarios.setSelectedItem(-1);
    }

    public void listarFabricados(ArrayList<Instrumentomateriales> fabricados) {
        vistaUsuario.dlmFabricaciones.clear();
        for (Instrumentomateriales fabricado : fabricados) {
            vistaUsuario.dlmFabricaciones.addElement(fabricado);
        }

        vistaUsuario.comboInstrumentos.removeAllItems();
        ArrayList<Instrumento> ins=modelo.getInstrumentos();
        vistaUsuario.comboMaterial.removeAllItems();
        ArrayList<Material> mat=modelo.getMateriales();
        for (Instrumento i: ins) {
            vistaUsuario.comboInstrumentos.addItem(i);
        }
        for (Material m:mat) {
            vistaUsuario.comboMaterial.addItem(m);
        }

        vistaUsuario.comboInstrumentos.setSelectedItem(-1);
        vistaUsuario.comboMaterial.setSelectedItem(-1);
    }

    public void listarPropietarioInstrumentos(List <Instrumento> lista){
        vistaUsuario.dlmInstrumentosPropietario.clear();
        for (Instrumento instrumento: lista) {
            vistaUsuario.dlmInstrumentosPropietario.addElement(instrumento);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vistaUsuario.listaInstrumentos) {
                Instrumento instrumento = (Instrumento) vistaUsuario.listaInstrumentos.getSelectedValue();
                vistaUsuario.txtCodigo.setText(String.valueOf(instrumento.getCodigo()));
                vistaUsuario.comboTipo.setSelectedItem(instrumento.getTipo());
                vistaUsuario.txtNombre.setText(String.valueOf(instrumento.getNombre()));
                vistaUsuario.txtPvp.setText(String.valueOf(instrumento.getPvp()));
                vistaUsuario.datePicker.setDate(instrumento.getFechaAdquisicion().toLocalDate());
                vistaUsuario.comboPropietarios.setSelectedItem(instrumento.getPropietario());
            }else if(e.getSource()== vistaUsuario.listaMarcas){
                Marca marca= (Marca) vistaUsuario.listaMarcas.getSelectedValue();
                vistaUsuario.txtNombreMarca.setText(String.valueOf(marca.getNombremarca()));
                vistaUsuario.comboPais.setSelectedItem(marca.getPais());
                vistaUsuario.txtDelegacion.setText(String.valueOf(marca.getDelegacion()));
                vistaUsuario.txtWeb.setText(String.valueOf(marca.getWeb()));
            }else if(e.getSource()== vistaUsuario.listaMateriales) {
                Material material= (Material) vistaUsuario.listaMateriales.getSelectedValue();
                vistaUsuario.txtNombreMaterial.setText(String.valueOf(material.getNombreMaterial()));
                vistaUsuario.comboCalidad.setSelectedItem(material.getCalidad());
                if (vistaUsuario.rbtnNo.isSelected()) vistaUsuario.rbtnNo.setText("No");
                if (vistaUsuario.rbtnSi.isSelected()) vistaUsuario.rbtnSi.setText("Si");
                vistaUsuario.txtProcedencia.setText(String.valueOf(material.getProcedencia()));
            }else if(e.getSource()== vistaUsuario.listaPropietarios){
                Propietario propietario= (Propietario) vistaUsuario.listaPropietarios.getSelectedValue();
                vistaUsuario.txtNombrePropietario.setText(String.valueOf(propietario.getNombre()));
                vistaUsuario.txtApellidos.setText(String.valueOf(propietario.getApellidos()));
                vistaUsuario.txtDccion.setText(propietario.getDccion());
                vistaUsuario.txtTfno.setText(String.valueOf(propietario.getTfno()));
            }else if(e.getSource()== vistaUsuario.listaFabricados){
                Instrumentomateriales fabricado= (Instrumentomateriales) vistaUsuario.listaFabricados.getSelectedValue();
                vistaUsuario.txtCantidad.setText(String.valueOf(fabricado.getCantidad()));
                vistaUsuario.comboInstrumentos.setSelectedItem(fabricado.getInstrumento());
                vistaUsuario.comboMaterial.setSelectedItem(fabricado.getMaterial());
            }
        }
    }

    @Override
    public void tableChanged(TableModelEvent e) {

        if (e.getType()==TableModelEvent.UPDATE) {
           if (e.getSource()== vistaUsuario.tablaClientes){
               int filaModificada=e.getFirstRow();
               try
           }

        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(e.getSource() == vistaUsuario.tablaClientes && vistaUsuario.tablaClientes.isEnabled()){
            int filaSeleccionada = vistaUsuario.tablaClientes.getSelectedRow();

            String nombre = (String)vistaUsuario.dtmClientes.getValueAt(filaSeleccionada,1);
            String cif = (String)vistaUsuario.dtmClientes.getValueAt(filaSeleccionada,2);
            int tfno = (int)vistaUsuario.dtmClientes.getValueAt(filaSeleccionada,3);
            String dccion = (String) vistaUsuario.dtmClientes.getValueAt(filaSeleccionada, 4);
            int cp = (int)vistaUsuario.dtmClientes.getValueAt(filaSeleccionada,5);
            String poblacion = (String) vistaUsuario.dtmClientes.getValueAt(filaSeleccionada, 6);
            String provincia = (String) vistaUsuario.dtmClientes.getValueAt(filaSeleccionada, 7);

            mostrarDatosClienteSeleccionado(nombre,cif,tfno,dccion, cp, poblacion, provincia);
        }else if(e.getSource() == vistaUsuario.tablaProveedores && vistaUsuario.tablaProveedores.isEnabled()){
            int filaSeleccionada = vistaUsuario.tablaProveedores.getSelectedRow();

            String nombre = (String)vistaUsuario.dtmProveedores.getValueAt(filaSeleccionada,1);
            String cif = (String)vistaUsuario.dtmProveedores.getValueAt(filaSeleccionada,2);
            int tfno = (int)vistaUsuario.dtmProveedores.getValueAt(filaSeleccionada,3);
            String dccion = (String) vistaUsuario.dtmProveedores.getValueAt(filaSeleccionada, 4);
            int cp = (int)vistaUsuario.dtmProveedores.getValueAt(filaSeleccionada,5);
            String poblacion = (String) vistaUsuario.dtmProveedores.getValueAt(filaSeleccionada, 6);
            String provincia = (String) vistaUsuario.dtmProveedores.getValueAt(filaSeleccionada, 7);

            mostrarDatosProveedorSeleccionado(nombre,cif,tfno,dccion, cp, poblacion, provincia);

        }else if (e.getSource() == vistaUsuario.tablaSemirremolques && vistaUsuario.tablaSemirremolques.isEnabled()){
            int filaSeleccionada = vistaUsuario.tablaSemirremolques.getSelectedRow();

            String matricula= (String) vistaUsuario.dtmSemirremolques.getValueAt(filaSeleccionada,1);
            DatePicker itv= (DatePicker) vistaUsuario.dtmSemirremolques.getValueAt(filaSeleccionada,2);
            ComboBox tipo= (ComboBox) vistaUsuario.dtmSemirremolques.getValueAt(filaSeleccionada,3);
            DatePicker seguro= (DatePicker) vistaUsuario.dtmSemirremolques.getValueAt(filaSeleccionada,4);

            mostrarDatosSemirremolqueSeleccionado(matricula,itv,tipo,seguro);
        }else if (e.getSource() == vistaUsuario.tablaTractoras && vistaUsuario.tablaTractoras.isEnabled()){
            int filaSeleccionada = vistaUsuario.tablaTractoras.getSelectedRow();

            String tractora= (String) vistaUsuario.dtmTractoras.getValueAt(filaSeleccionada,1);
            DatePicker itv= (DatePicker) vistaUsuario.dtmTractoras.getValueAt(filaSeleccionada,2);
            DatePicker limitador= (DatePicker) vistaUsuario.dtmTractoras.getValueAt(filaSeleccionada,3);
            DatePicker tacografo= (DatePicker) vistaUsuario.dtmTractoras.getValueAt(filaSeleccionada,4);
            DatePicker seguro= (DatePicker) vistaUsuario.dtmTractoras.getValueAt(filaSeleccionada,5);
            int tarjetaTte= (int) vistaUsuario.dtmTractoras.getValueAt(filaSeleccionada,6);

            mostrarDatosTractoraSeleccionada(tractora,itv,limitador,tacografo,seguro,tarjetaTte);
        }else if (e.getSource() == vistaUsuario.tablaViajes && vistaUsuario.tablaViajes.isEnabled()){
            int filaSeleccionada = vistaUsuario.tablaViajes.getSelectedRow();

            ComboBox matricula= (ComboBox) vistaUsuario.dtmViajes.getValueAt(filaSeleccionada, 1);
            ComboBox semirremolque= (ComboBox) vistaUsuario.dtmViajes.getValueAt(filaSeleccionada, 2);
            String albaran= (String) vistaUsuario.dtmViajes.getValueAt(filaSeleccionada,3);
            DatePicker fecha= (DatePicker) vistaUsuario.dtmViajes.getValueAt(filaSeleccionada,4);
            ComboBox chofer = (ComboBox) vistaUsuario.dtmViajes.getValueAt(filaSeleccionada,5);
            double precio= (double) vistaUsuario.dtmViajes.getValueAt(filaSeleccionada,6);
            String origen = (String) vistaUsuario.dtmViajes.getValueAt(filaSeleccionada,7);
            String destino = (String) vistaUsuario.dtmViajes.getValueAt(filaSeleccionada,8);
            ComboBox cliente = (ComboBox) vistaUsuario.dtmViajes.getValueAt(filaSeleccionada,9);
            double peso= (double) vistaUsuario.dtmViajes.getValueAt(filaSeleccionada,10);

            mostrarDatosViajeSeleccionado(matricula,semirremolque,albaran,fecha,chofer,precio,origen, destino,
                    cliente, peso);

        } else {
            int filaSeleccionada = vistaUsuario.tablaVendidos.getSelectedRow();

            ComboBox nombreProveedor= (ComboBox) vistaUsuario.dtmVendidos.getValueAt(filaSeleccionada, 1);
            ComboBox cliente = (ComboBox) vistaUsuario.dtmVendidos.getValueAt(filaSeleccionada, 2);
            String matricula= (String) vistaUsuario.dtmVendidos.getValueAt(filaSeleccionada, 3);
            DatePicker fecha= (DatePicker) vistaUsuario.dtmVendidos.getValueAt(filaSeleccionada, 4);
            String semirremolque = (String) vistaUsuario.dtmVendidos.getValueAt(filaSeleccionada, 5);
            String origen = (String) vistaUsuario.dtmVendidos.getValueAt(filaSeleccionada,6);
            String destino = (String) vistaUsuario.dtmVendidos.getValueAt(filaSeleccionada,7);
            String albaran= (String) vistaUsuario.dtmVendidos.getValueAt(filaSeleccionada,8);
            double peso= (double) vistaUsuario.dtmVendidos.getValueAt(filaSeleccionada,9);
            double precio= (double) vistaUsuario.dtmVendidos.getValueAt(filaSeleccionada,10);

            mostrarDatosVendidoSeleccionado(nombreProveedor, cliente, matricula, fecha, semirremolque, origen, destino,
                    albaran, peso, precio);
        }
    }

    private void mostrarDatosClienteSeleccionado(String nombre, String cif, int tfno, String dccion, int cp,
                                                 String poblacion, String provincia) {
        vistaUsuario.txtNombreCliente.setText(nombre);
        vistaUsuario.txtCifCliente.setText(cif);
        vistaUsuario.txtTfnoCliente.setText(String.valueOf(tfno));
        vistaUsuario.txtDccionCliente.setText(dccion);
        vistaUsuario.txtCPCliente.setText(String.valueOf(cp));
        vistaUsuario.txtPoblacionCliente.setText(poblacion);
        vistaUsuario.txtProvinciaCliente.setText(provincia);
    }

    private void mostrarDatosProveedorSeleccionado(String nombre, String cif, int tfno, String dccion, int cp,
                                                   String poblacion, String provincia) {
        vistaUsuario.txtNombreProveedor.setText(nombre);
        vistaUsuario.txtCifProveedor.setText(cif);
        vistaUsuario.txtTfnoProveedor.setText(String.valueOf(tfno));
        vistaUsuario.txtDccionProveedor.setText(dccion);
        vistaUsuario.txtCPProveedor.setText(String.valueOf(cp));
        vistaUsuario.txtPoblacionProveedor.setText(poblacion);
        vistaUsuario.txtProvinciaProveedor.setText(provincia);
    }

    private void mostrarDatosSemirremolqueSeleccionado(String matricula, DatePicker itv, ComboBox tipo,
                                                       DatePicker seguro) {
        vistaUsuario.txtMatriculaSemi.setText(matricula);
        vistaUsuario.fechaITVSemi.setText(String.valueOf(itv));
        vistaUsuario.comboTipoSemi.setSelectedItem(String.valueOf(tipo));
        vistaUsuario.fechaSeguroSemi.setText(String.valueOf(seguro));
    }

    private void mostrarDatosTractoraSeleccionada(String tractora, DatePicker itv, DatePicker limitador,
                                                  DatePicker tacografo, DatePicker seguro, int tarjetaTte) {
        vistaUsuario.txtMatriculaTractora.setText(tractora);
        vistaUsuario.fechaITVTractora.setText(String.valueOf(itv));
        vistaUsuario.fechaLimitador.setText(String.valueOf(limitador));
        vistaUsuario.fechaTacografo.setText(String.valueOf(tacografo));
        vistaUsuario.fechaSeguroTractora.setText(String.valueOf(tacografo));
        vistaUsuario.txtTfnoCliente.setText(String.valueOf(seguro));
        vistaUsuario.txtTarjetaTte.setText((String.valueOf(tarjetaTte)));
    }

    private void mostrarDatosViajeSeleccionado(ComboBox matricula, ComboBox semirremolque, String albaran,
                                               DatePicker fecha, ComboBox chofer, double precio, String origen,
                                               String destino, ComboBox cliente, double peso) {
        vistaUsuario.comboMatricula.setSelectedItem(String.valueOf(matricula));
        vistaUsuario.comboSemi.setSelectedItem(String.valueOf(semirremolque));
        vistaUsuario.txtAlbaran.setText(String.valueOf(albaran));
        vistaUsuario.fechaViaje.setText(String.valueOf(fecha));
        vistaUsuario.comboChofer.setSelectedItem(String.valueOf(chofer));
        vistaUsuario.txtPrecio.setText(String.valueOf(precio));
        vistaUsuario.txtOrigen.setText(origen);
        vistaUsuario.txtDestino.setText(destino);
        vistaUsuario.comboCliente.setSelectedItem(String.valueOf(cliente));
        vistaUsuario.txtPeso.setText(String.valueOf(peso));
    }

    private void mostrarDatosVendidoSeleccionado(ComboBox nombreProveedor, ComboBox cliente, String matricula, DatePicker fecha, String semirremolque, String origen, String destino, String albaran, double peso, double precio) {
        vistaUsuario.comboProveedor.setSelectedItem(String.valueOf(nombreProveedor));
        vistaUsuario.comboClienteProveedor.setSelectedItem(String.valueOf(cliente));
        vistaUsuario.txtMatriculaViajeProveedor.setText(matricula);
        vistaUsuario.fechaViajeProveedor.setText(String.valueOf(fecha));
        vistaUsuario.txtMatriculaSemiViajeProveedor.setText(semirremolque);
        vistaUsuario.txtOrigenProveedor.setText(origen);
        vistaUsuario.txtDestinoProveedor.setText(destino);
        vistaUsuario.txtAlbaranProveedor.setText(albaran);
        vistaUsuario.txtPrecioProveedor.setText(String.valueOf(precio));
        vistaUsuario.txtPesoProveedor.setText(String.valueOf(peso));
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}

