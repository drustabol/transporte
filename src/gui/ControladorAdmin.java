package gui;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

public class ControladorAdmin implements ActionListener, ListSelectionListener {
    
    private VistaAdmin vistaAdmin;
    private Modelo modelo;

    public ControladorAdmin(Modelo modelo, VistaAdmin vistaAdmin) {

        this.vistaAdmin = vistaAdmin;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener((MouseListener) this);
    }

    private void addListSelectionListener(MouseListener mouseListener) {
    }

    private void addActionListeners(ActionListener listener) {
        vistaAdmin.btnAddChofer.addActionListener(listener);
        vistaAdmin.btnModChofer.addActionListener(listener);
        vistaAdmin.btnDelChofer.addActionListener(listener);
        vistaAdmin.btnListChofer.addActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
