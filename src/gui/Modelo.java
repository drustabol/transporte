package gui;

import hibernate.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.ArrayList;


public class Modelo {

    public static SessionFactory sessionFactory;

    public void desconectar() {
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    public void conectar() {
        Configuration configuracion = new Configuration();
        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Chofer.class);
        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Proveedor.class);
        configuracion.addAnnotatedClass(Semirremolque.class);
        configuracion.addAnnotatedClass(Tractora.class);
        configuracion.addAnnotatedClass(Viajespropios.class);
        configuracion.addAnnotatedClass(Viajesvendidos.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(configuracion.getProperties()).build();
        sessionFactory = configuracion.buildSessionFactory(ssr);
    }

    ArrayList<Chofer> getChoferes() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Chofer");
        ArrayList<Chofer> lista = (ArrayList<Chofer>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Cliente> getClientes(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente");
        ArrayList<Cliente> lista = (ArrayList<Cliente>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Proveedor> getProveedores(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Proveedor ");
        ArrayList<Proveedor> lista = (ArrayList<Proveedor>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Semirremolque> getSemirremolques(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Semirremolque ");
        ArrayList<Semirremolque> lista = (ArrayList<Semirremolque>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Tractora> getTractora(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Tractora ");
        ArrayList<Tractora> lista = (ArrayList<Tractora>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Viajespropios> getViajespropios(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Viajespropios ");
        ArrayList<Viajespropios> lista = (ArrayList<Viajespropios>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<Viajesvendidos> getViajesvendidos(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Viajesvendidos ");
        ArrayList<Viajesvendidos> lista = (ArrayList<Viajesvendidos>) query.getResultList();
        sesion.close();
        return lista;
    }

    void insertar(Object o) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(o);
        sesion.getTransaction().commit();

        sesion.close();
    }

    void modificar(Object o) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(o);
        sesion.getTransaction().commit();
        sesion.close();
    }

    void eliminar(Object o) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(o);
        sesion.getTransaction().commit();
        sesion.close();
    }

}
